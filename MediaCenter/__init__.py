# -*- coding: utf-8 -*-
from Components.Language import language
from Tools.Directories import resolveFilename, SCOPE_PLUGINS, SCOPE_LANGUAGE
from os import environ as os_environ
import gettext

import Plugins.Plugin
from Components.config import config
from Components.config import ConfigSubsection
from Components.config import ConfigSelection
from Components.config import ConfigInteger
from Components.config import ConfigSubList
from Components.config import ConfigSubDict
from Components.config import ConfigText
from Components.config import configfile
from Components.config import ConfigYesNo
from skin import loadSkin

currentmcversion = "100"
currentmcplatform = "sh4"

config.plugins.mc_favorites = ConfigSubsection()
config.plugins.mc_favorites.foldercount = ConfigInteger(0)
config.plugins.mc_favorites.folders = ConfigSubList()

config.plugins.mc_globalsettings = ConfigSubsection()
config.plugins.mc_globalsettings.language = ConfigSelection(default="EN", choices = [("EN", _("Deutsch!"))])
config.plugins.mc_globalsettings.showinmainmenu = ConfigYesNo(default=True)
config.plugins.mc_globalsettings.showinextmenu = ConfigYesNo(default=True)
config.plugins.mc_globalsettings.currentversion = ConfigInteger(0, (0, 999))
config.plugins.mc_globalsettings.currentplatform = ConfigText(default = currentmcplatform)

config.plugins.mc_globalsettings.dst_top = ConfigInteger(0, (0, 999))
config.plugins.mc_globalsettings.dst_left = ConfigInteger(0, (0, 999))
config.plugins.mc_globalsettings.dst_width = ConfigInteger(720, (1, 720))
config.plugins.mc_globalsettings.dst_height = ConfigInteger(576, (1, 576))

config.plugins.mc_globalsettings.currentskin = ConfigSubsection()
config.plugins.mc_globalsettings.currentskin.path = ConfigText(default = "BlackWhiteHD/skin.xml")

config.plugins.mc_globalsettings.currentversion.value = currentmcversion
config.plugins.mc_globalsettings.currentplatform.value = currentmcplatform

# Load Skin
import os.path
if os.path.exists("/usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/skins/" + config.plugins.mc_globalsettings.currentskin.path.value):
	loadSkin("/usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/skins/" + config.plugins.mc_globalsettings.currentskin.path.value)
else:
	loadSkin("/usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/skins/BlackWhiteHD/skin.xml")

# Favorite Folders
def addFavoriteFolders():
	i = len(config.plugins.mc_favorites.folders)
	config.plugins.mc_favorites.folders.append(ConfigSubsection())
	config.plugins.mc_favorites.folders[i].name = ConfigText("", False)
	config.plugins.mc_favorites.folders[i].basedir = ConfigText("/", False)
	config.plugins.mc_favorites.foldercount.value = i+1
	return i

for i in range(0, config.plugins.mc_favorites.foldercount.value):
	addFavoriteFolders()



def localeInit():
	lang = language.getLanguage()[:2] # getLanguage returns e.g. "fi_FI" for "language_country"
	os_environ["LANGUAGE"] = lang # Enigma doesn't set this (or LC_ALL, LC_MESSAGES, LANG). gettext needs it!
	gettext.bindtextdomain("SHOUTcast", resolveFilename(SCOPE_PLUGINS, "Extensions/MediaCenter/locale"))

def _(txt):
	t = gettext.dgettext("SHOUTcast", txt)
	if t == txt:
		# print "[SHOUTcast] fallback to default translation for", txt
		t = gettext.gettext(txt)
	return t

localeInit()
language.addCallback(localeInit)

