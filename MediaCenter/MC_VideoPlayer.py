# -*- coding: iso-8859-15 -*-

from enigma import iPlayableService, eTimer, eWidget, eRect, eServiceReference, iServiceInformation, ePicLoad
from Screens.Screen import Screen
from Screens.ServiceInfo import ServiceInfoList, ServiceInfoListEntry
from Screens.HelpMenu import HelpableScreen
from Screens.MessageBox import MessageBox
from Components.ActionMap import ActionMap, NumberActionMap, HelpableActionMap
from Components.Pixmap import Pixmap, MovingPixmap
from Components.Label import Label
from Components.Button import Button
from Components.ServiceEventTracker import ServiceEventTracker
from Components.ConfigList import ConfigList, ConfigListScreen
from Components.config import *

from Tools.Directories import resolveFilename, fileExists, pathExists, createDir, SCOPE_MEDIA
from Components.FileList import FileList
from Components.AVSwitch import AVSwitch
from Screens.InfoBar import MoviePlayer
from Plugins.Plugin import PluginDescriptor

from GlobalFunctions import MC_FolderOptions, MC_FavoriteFolders, MC_FavoriteFolderAdd, MC_FavoriteFolderEdit, MC_VideoInfoView

import os
from os import path as os_path

from GoogleSearchParser import *

config.plugins.mc_vp = ConfigSubsection()
config.plugins.mc_vp.showPreview = ConfigYesNo(default=True)
config.plugins.mc_vp.preview_delay = ConfigInteger(default=5, limits=(1, 99))
config.plugins.mc_vp.lastDir = ConfigText(default=resolveFilename(SCOPE_MEDIA))
config.plugins.mc_vp.thumbpath = ConfigSelection(default = "/usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/Bilder/", choices = [("/media/sdb1/", _("/media/sdb1/")), ("/usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/Bilder/", _("/usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/Bilder/")), ("/media/sda4/", _("/media/sda4/"))])

def getAspect():
	val = AVSwitch().getAspectRatioSetting()
	return val/2

#------------------------------------------------------------------------------------------

class MC_VideoPlayer(Screen, HelpableScreen):
	def __init__(self, session):
		Screen.__init__(self, session)
		HelpableScreen.__init__(self)
		
		self.isVisible = True
		self.LoadPictureFile = ""
		self.PictureDir = ""
		self.oldService = self.session.nav.getCurrentlyPlayingServiceReference()
		self.session.nav.stopService()
		self.showPreviewState = False
		self.Scale = AVSwitch().getFramebufferScale()
		self.PicLoad=ePicLoad()
		# Show Background MVI
		#os.system("/usr/bin/showiframe /usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/icons/background.mvi &")
		
		self["key_red"] = Button(_("Favoriten"))
		self["key_green"] = Button(_("Vorschau"))
		self["key_yellow"] = Button(_("Pic Download"))
		self["key_blue"] = Button(_("Einstellungen"))
		self["Titel"] = Label("VideoPlayer")

		self["currentfolder"] = Label("")
		self["currentfavname"] = Label("")
		self.curfavfolder = -1
		
		self["actions"] = HelpableActionMap(self, "MC_VideoPlayerActions", 
			{
				"ok": (self.KeyOk, "Abspielen"),
				"cancel": (self.Exit, "Video Player beenden"),
				"left": (self.leftUp, "Zum Anfang"),
				"right": (self.rightDown, "Zum Ende"),
				"up": (self.up, "Liste rauf"),
				"down": (self.down, "Liste runter"),
				"menu": (self.KeyMenu, "Datei / Pfad Optionen"),
				"video": (self.visibility, "Player ein-/ausblenden"),
				"info": (self.showFileInfo, "Zeige Datei Info"),
				"nextBouquet": (self.NextFavFolder, "Naechster Favorit"),
				"prevBouquet": (self.PrevFavFolder, "Vorheriger Favorit"),
				"stop": (self.StopPlayback, "Wiedergabe beenden"),
				"play": (self.KeyOk, "Wiedergabe starte"),
				"red": (self.FavoriteFolders, "Favoriten"),
				"green": (self.changePreviewState, "Vorschau An/Aus"),
				"blue": (self.KeySettings, "Einstellungen"),
				"yellow": (self.DownloadMSG, "Download"),
			}, -2)
		
		self.aspect = getAspect()
		currDir = config.plugins.mc_vp.lastDir.value
		if not pathExists(currDir):
			currDir = "/"

		self["currentfolder"].setText(str(currDir))
		self.filelist = FileList(currDir, useServiceRef = True, showDirectories = True, showFiles = True, matchingPattern = "(?i)^.*\.(ts|vob|mpg|mpeg|avi|mkv|dat|iso|mp4|divx|flv|mov)")
		self["filelist"] = self.filelist
		self["thumbnail"] = Pixmap()
		
		self.ThumbTimer = eTimer()
		self.ThumbTimer.callback.append(self.showThumb)
		self.ThumbPictureTimer = eTimer()
		self.ThumbPictureTimer.callback.append(self.showThumbPicture)
		self.PicLoad.PictureData.get().append(self.DecodePicture)
		self.onLayoutFinish.append(self.showThumbPicture)
		#self.ThumbTimer.start(500, True)

		self.__event_tracker = ServiceEventTracker(screen=self, eventmap=
			{
				iPlayableService.evUser+11: self.__evDecodeError,
				iPlayableService.evUser+12: self.__evPluginError
			})

	def up(self):
		self["filelist"].up()
		self.ThumbTimer.start(config.plugins.mc_vp.preview_delay.getValue() * 1000, True)
		self.ThumbPictureTimer.start(config.plugins.mc_vp.preview_delay.getValue() * 100, True)

	def down(self):
		self["filelist"].down()
		self.ThumbTimer.start(config.plugins.mc_vp.preview_delay.getValue() * 1000, True)
		self.ThumbPictureTimer.start(config.plugins.mc_vp.preview_delay.getValue() * 100, True)
		
	def leftUp(self):
		self["filelist"].pageUp()
		self.ThumbTimer.start(config.plugins.mc_vp.preview_delay.getValue() * 1000, True)
		self.ThumbPictureTimer.start(config.plugins.mc_vp.preview_delay.getValue() * 100, True)
		
	def rightDown(self):
		self["filelist"].pageDown()
		self.ThumbTimer.start(config.plugins.mc_vp.preview_delay.getValue() * 1000, True)
		self.ThumbPictureTimer.start(config.plugins.mc_vp.preview_delay.getValue() * 100, True)

	def NextFavFolder(self):
		if self.curfavfolder + 1 < config.plugins.mc_favorites.foldercount.value:
			self.curfavfolder += 1
			self.favname = config.plugins.mc_favorites.folders[self.curfavfolder].name.value
			self.folder = config.plugins.mc_favorites.folders[self.curfavfolder].basedir.value
			self["currentfolder"].setText(("%s") % (self.folder))
			self["currentfavname"].setText(("%s") % (self.favname))
			if os.path.exists(self.folder) == True:
				self["filelist"].changeDir(self.folder)
		else:
			return
			
	def PrevFavFolder(self):
		if self.curfavfolder <= 0:
			return
		else:
			self.curfavfolder -= 1
			self.favname = config.plugins.mc_favorites.folders[self.curfavfolder].name.value
			self.folder = config.plugins.mc_favorites.folders[self.curfavfolder].basedir.value
			self["currentfolder"].setText(("%s") % (self.folder))
			self["currentfavname"].setText(("%s") % (self.favname))
			if os.path.exists(self.folder) == True:
				self["filelist"].changeDir(self.folder)

	def changePreviewState(self):
		if self.showPreviewState:
			self.showPreviewState = False
		else:
			self.showPreviewState = True
		return self.showPreview()

	def showPreview(self):
		if self.showPreviewState:
			if self["filelist"].canDescent():
				return
			else:
				if self["filelist"].getServiceRef() is not None:
					self.session.nav.stopService()
					self.session.nav.playService(self["filelist"].getServiceRef())
		else:
			self.session.nav.stopService()

	def DownloadMSG(self):
		self.session.openWithCallback(self.callMsgDownload, MessageBox, _("Bilder Download Starten ?\nDieser Download kann einige Minuten in Anspruch nehmen!\nBitte wartet bis der Spinner wieder verschwunden ist.\n"), MessageBox.TYPE_YESNO)

	def callMsgDownload(self, result):
		if result:
			self.DownloadPicture()
		else:
			pass

	def DownloadPicture(self):
		find = ['.ts','.vob','.mpg','.mpeg','.avi','.mkv','.dat','.iso','.mp4','.divx','.flv','.mov']
		PictureDir = config.plugins.mc_vp.thumbpath.value
		currDir = self.filelist.getCurrentDirectory()
		if currDir == None:
			currDir = "/"
		# is the currDir + Filename
		#name = self.filelist.getFilename()
		FileListfromDir = os.listdir(currDir)
		List = []
		for x in FileListfromDir:
			#set blacklistet file end
			if x.find('.ap') != -1 or x.find('.sc') != -1 or x.find('.cuts') != -1 or x.find('.meta') != -1 or x.find('.eit') != -1 or x.find('.xml') != -1 or x.find('.pkl') != -1:
				pass
			else:
				for y in find:
					# find only video files, no dirs
					if x.find(y) != -1:
						List.append(x)
						print "[MediaCenter] Add file to List = %s" % x
		for name in List:
		  	print "[MediaCenter] Download file what is in List = %s" % name
			# Filename is the name of the local file for config file
			FileName = name
			if FileName.find("(") != -1 and FileName.find(")") != -1:
				FileName = FileName.split("(",1)[0]
				if FileName.find(" ") != -1:
					FileName = FileName.strip()
			if FileName.find("\'") != -1:
				FileName = FileName.replace("\'","")
			if FileName.find("´") != -1:
				FileName = FileName.replace("´","")
			if FileName.find("`") != -1:
				FileName = FileName.replace("`","")
			# name ist the name for the Picture File for the Config File
			# is the Date befor the name split the name
			if name.find("-") != -1:
				name = name.split("-",1)[1]
			# fixed names whith '
			if name.find("\'") != -1:
				name = name.replace("\'","")
			if name.find("´") != -1:
				name = name.replace("´","")
			if name.find("`") != -1:
				name = name.replace("`","")
			# find _ and make a leerzeichen
			if name.find("_") != -1:
				name = name.replace("_"," ")
			# split the File end of list
			for x in find:
				if name.find(x) != -1:
					name = name.split(x)[0]
			if name.find("(") != -1 and name.find(")") != -1:
				name = name.split("(",1)[0]
			# for online search need alle Leerzeichen change to +
			if name.find(" ") != -1:
				name = name.strip()
				name = name.replace(" ","+")
			# result is only the Moviename
			print '[FileName] = %s' % name
			# is file found and FileName in File break the Download
			# so we can download only needet picture
			DOWNLOAD = True
			try:
				f = open(PictureDir + 'Picture.conf', 'r')
				for x in f:
					if x.find(FileName) != -1:
						DOWNLOAD = False
			except:
				print "No Picture.conf Found, start Download..."
			if DOWNLOAD:
				# we use the GoogleSearchParser search funktion to find the download url of the Picture
				for url in search(name, tld='de', lang='de', tbs='0', safe='off', num=1, start=0,
			  stop=1, pause=1.0, only_standard=False):
					print '[DownloadURL] = %s' % url
					# Change the + to _ and save after the Picture by name ;)
					if name.find("+") != -1:
						name = name.replace("+","_")
					# wget download peer url the Picture to name of file
					os.system('wget -O ' + PictureDir + name + '.jpg ' + url)
				# create a config file for name and picture
				os.system('echo ' + FileName + ':' + name + ' >> ' + PictureDir + 'Picture.conf')

	def showThumbPicture(self):
		self.PictureFile = None
		self.PictureDir = config.plugins.mc_vp.thumbpath.value
		self.IconDir = "/usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/icons/"
		VideoFile = self.filelist.getFilename()
		currDir = self.filelist.getCurrentDirectory()
		# find the name of file
		try:
			FileName = VideoFile.split(currDir)
			# Find File name after Dir
			FileName = FileName[1]
			# remove special types
			if FileName.find("(") != -1 and FileName.find(")") != -1:
				FileName = FileName.split("(",1)[0]
				if FileName.find(" ") != -1:
					FileName = FileName.strip()
			if FileName.find("\'") != -1:
				FileName = FileName.replace("\'","")
			if FileName.find("´") != -1:
				FileName = FileName.replace("´","")
			if FileName.find("`") != -1:
				FileName = FileName.replace("`","")
		except:
			return
		# find the PictureFile
		try:
			f = open(self.PictureDir + 'Picture.conf', 'r')
		except:
			print 'Error no File FOUND'
			return
		for x in f:
			if x.find(FileName) != -1:
				self.PictureFile = x.split(":")[1].replace("\n","")
		if self.PictureFile == None:
			# if no Filename so is a Dir ;)
			if FileName.find('/'):
				self.PictureDir = self.IconDir
				self.PictureFile = 'Ordner'
			else:
				self.PictureDir = self.IconDir
				self.PictureFile = 'NoPic'
		self.LoadPictureFile = self.PictureDir + self.PictureFile + '.jpg'
		print '[PicFile] = ' + self.LoadPictureFile
		self.PicLoad.setPara([
					self["thumbnail"].instance.size().width(),
					self["thumbnail"].instance.size().height(),
					self.Scale[0],
					self.Scale[1],0,1,"#002C2C39"])
		self.PicLoad.startDecode(self.LoadPictureFile)
			
	def DecodePicture(self, PicInfo=""):
		ptr = self.PicLoad.getData()
		self["thumbnail"].instance.setPixmap(ptr)

	def showThumb(self):
		if self.showPreviewState:
			if config.plugins.mc_vp.showPreview.getValue() == False:
				return
			
			if self["filelist"].canDescent():
				return
			else:
				if self["filelist"].getServiceRef() is not None:
					self.session.nav.stopService()
					self.session.nav.playService(self["filelist"].getServiceRef())
					#self.ShowFileInfoTimer.start(3000, True)
		else:
			return

	def showFileInfo(self):
		if self["filelist"].canDescent():
			return
		else:
			self.session.open(MC_VideoInfoView, self["filelist"].getCurrentDirectory() + self["filelist"].getFilename() , self["filelist"].getFilename(), self["filelist"].getServiceRef())
	
	def KeyOk(self):
		if self.isVisible == False:
			self.visibility()
			return
		
		self.ThumbTimer.stop()
		
		if self.filelist.canDescent():
			self.filelist.descent()
		else:
			self.session.open(MoviePlayer, self["filelist"].getServiceRef())
			
			# screen adjustment
			os.system("echo " + hex(config.plugins.mc_globalsettings.dst_top.value)[2:] + " > /proc/stb/vmpeg/0/dst_top")
			os.system("echo " + hex(config.plugins.mc_globalsettings.dst_left.value)[2:] + " > /proc/stb/vmpeg/0/dst_left")
			os.system("echo " + hex(config.plugins.mc_globalsettings.dst_width.value)[2:] + " > /proc/stb/vmpeg/0/dst_width")
			os.system("echo " + hex(config.plugins.mc_globalsettings.dst_height.value)[2:] + " > /proc/stb/vmpeg/0/dst_height")

	def KeyMenu(self):
		self.ThumbTimer.stop()
		if self["filelist"].canDescent():
			if self.filelist.getCurrent()[0][1]:
				self.currentDirectory = self.filelist.getCurrent()[0][0]
				if self.currentDirectory is not None:
					foldername = self.currentDirectory.split('/')
					foldername = foldername[-2]
					self.session.open(MC_FolderOptions,self.currentDirectory, foldername)

	def StartThumb(self):
		self.session.openWithCallback(self.returnVal, ThumbView, self.filelist.getFileList(), self.filelist.getFilename(), self.filelist.getCurrentDirectory())

	def returnVal(self, val=0):
		if val > 0:
			for x in self.filelist.getFileList():
				if x[0][1] == True:
					val += 1
			self.filelist.moveToIndex(val)

	def StartExif(self):
		if not self.filelist.canDescent():
			self.session.open(ExifView, self.filelist.getCurrentDirectory() + self.filelist.getFilename(), self.filelist.getFilename())

	def visibility(self, force=1):
		if self.isVisible == True:
			self.isVisible = False
			self.hide()
		else:
			self.isVisible = True
			self.show()
			#self["list"].refresh()

	def StopPlayback(self):
		self.ThumbTimer.stop()
		self.session.nav.stopService()
		self.session.nav.playService(self.oldService)
			
		if self.isVisible == False:
			self.show()
			self.isVisible = True

	def JumpToFolder(self, jumpto = None):
		if jumpto is None:
			return
		else:
			self["filelist"].changeDir(jumpto)
			self["currentfolder"].setText(("%s") % (jumpto))
			
	def FavoriteFolders(self):
		self.session.openWithCallback(self.JumpToFolder, MC_FavoriteFolders)
	
	def KeySettings(self):
		self.session.open(VideoPlayerSettings)

	def KeyScreenAdjust(self):
		if self.isVisible == True:
			return
			
		os.system("echo " + hex(config.plugins.mc_globalsettings.dst_top.value)[2:] + " > /proc/stb/vmpeg/0/dst_top")
		os.system("echo " + hex(config.plugins.mc_globalsettings.dst_left.value)[2:] + " > /proc/stb/vmpeg/0/dst_left")
		os.system("echo " + hex(config.plugins.mc_globalsettings.dst_width.value)[2:] + " > /proc/stb/vmpeg/0/dst_width")
		os.system("echo " + hex(config.plugins.mc_globalsettings.dst_height.value)[2:] + " > /proc/stb/vmpeg/0/dst_height")

	def __evDecodeError(self):
		currPlay = self.session.nav.getCurrentService()
		sVideoType = currPlay.info().getInfoString(iServiceInformation.sVideoType)
		print "[__evDecodeError] video-codec %s can't be decoded by hardware" % (sVideoType)
		self.session.open(MessageBox, _("Kann keine %s Video-Streams wiedergeben!") % sVideoType, type = MessageBox.TYPE_INFO,timeout = 10 )

	def __evPluginError(self):
		currPlay = self.session.nav.getCurrentService()
		message = currPlay.info().getInfoString(iServiceInformation.sUser+12)
		print "[__evPluginError]" , message
		self.session.open(MessageBox, ("GStreamer Fehler: %s fehlt") % message, type = MessageBox.TYPE_INFO,timeout = 20 )
 
	def Exit(self):
		if self.isVisible == False:
			self.visibility()
			return
			
		if self["filelist"].getCurrentDirectory() is None:
			config.plugins.mc_vp.lastDir.value = "/"
		else:
			config.plugins.mc_vp.lastDir.value = self.filelist.getCurrentDirectory()

		self.session.nav.stopService()
		#self.session.nav.playService(self.oldService)
		
		config.plugins.mc_vp.save()
		self.session.nav.playService(self.oldService)
		self.close()
		
		
		
#------------------------------------------------------------------------------------------

class VideoPlayerSettings(Screen, ConfigListScreen):
	skin = """
		<screen position="160,220" size="700,120" title="Media Center - VideoPlayer Settings" >
			<widget name="config" position="10,10" size="680,100" />
		</screen>"""
		
	def __init__(self, session):
		Screen.__init__(self, session)

		self["actions"] = NumberActionMap(["SetupActions","OkCancelActions"],
		{
			"ok": self.keyOK,
			"cancel": self.keyOK
		}, -1)
		
		self.list = []
		self.list.append(getConfigListEntry(_("Autoplay Aktiv"), config.plugins.mc_vp.showPreview))
		self.list.append(getConfigListEntry(_("Autoplay Verzoegerung"), config.plugins.mc_vp.preview_delay))
		self.list.append(getConfigListEntry(_("Bilder Verzeichniss"),config.plugins.mc_vp.thumbpath))
		ConfigListScreen.__init__(self, self.list, session)
		
	def keyOK(self):
		config.plugins.mc_vp.save()
		self.close()		

