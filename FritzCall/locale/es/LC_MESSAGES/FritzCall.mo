��    w      �  �   �      
     
     )
     @
  	   D
     N
     e
     {
     �
  )   �
     �
     �
     �
     �
     �
  0        5  /   P  )   �     �     �     �     �     �  %   �     	          2     G     ^  =   g     �     �     �     �  $   �  "        +     @     F     M     ]     e  $   �     �     �     �     �     �     �     �                    $     1     6     :     S     e     w     �     �     �     �     �     �     �               3     ?  	   R     \     a       /   �     �     �     �     �     �               5     >     R     e     {     �     �  (   �     �     �     �     �     
  F     �   X     �     �            	   "  	   ,     6     ;     A     J     ]     n     u  	   �     �     �     �     �     �     �  n  �     =     M     i     o     ~     �     �     �  0   �     �               #     3  +   J     v  9   �  8   �          	     %     -     4  0   C     t     �     �     �     �  6   �     +     2  !   N     p  -   �  &   �     �     �     �     �       (   !  *   J     u     �     �     �     �     �     �     �     �     �          $     +      1     R     n     �  1   �  '   �     �          
     '     C     J  "   ]     �     �     �     �     �     �  ;   �          1     9     @     \     j  )   ~     �     �     �     �          #     )  :   1     l     �     �     �     �  G   �  �         �     �     �     �  
   �  
                  !     4     M     ]     d  
   r     }     �     �     �     �     �         K      o           R   j      0      _   v   $   g   -   	             "   q                 s              Y   N   =   J             +      p       i       f       L          (       @          4   1          T   r           6   U          W   ,   V       %   O      Q          ;       '          m   <   w   G   2   B   ?   c   [   d   e   P   M   a   b              *       I   `              &   ]   n       k   X       D          8   3       H      9   ^       u   7      \                 F          :   !           A       )       .       C   l           #   h   /      Z   5      t       >       
   S   E    About FritzCall Add entry to phonebook All All calls Append shortcut number Append type of number Append vanity name Austria Automatically add new Caller to PhoneBook Call Call monitoring Cancel Connected since Connected to FRITZ!Box! Connecting to FRITZ!Box failed
 (%s)
retrying... Connecting to FRITZ!Box... Connection to FRITZ!Box! lost
 (%s)
retrying... Could not parse FRITZ!Box Phonebook entry Country DECT phones registered Debug Delete Delete entry Display FRITZ!box-Fon calls on screen Display all calls Display incoming calls Display missed calls Display outgoing calls Do what? Do you really want to delete entry for

%(number)s

%(name)s? Edit Edit selected entry Enter Search Terms Entry incomplete. Extension number to initiate call on FRITZ!Box FON address (Name or IP) FRITZ!Box Fon Status Flash France FritzCall Setup Germany Getting calls from FRITZ!Box... Getting status from FRITZ!Box Fon... IP Address: Incoming Incoming calls Italy Last 10 calls:
 Lookup MSN to show (separated by ,) Mailbox Missed Missed calls Mute on call Name New No DECT phone registered No entry selected No mailbox active No result from LDIF No result from Outlook export No result from reverse lookup Number OK One DECT phone registered One mailbox active Outgoing Outgoing calls Password Accessing FRITZ!Box Phone calls PhoneBook Location Phonebook Quit Read PhoneBook from FRITZ!Box Reset Reverse Lookup Caller ID (select country below) Reverse searching... Save Search Search (case insensitive) Search phonebook Searching in LDIF... Searching in Outlook export... Shortcut Show Outgoing Calls Show after Standby Show details of entry Strip Leading 0 Switzerland The Netherlands Timeout for Call Notifications (seconds) Toggle Mailbox Toggle WLAN UNKNOWN Use internal PhoneBook Vanity You need to enable the monitoring on your FRITZ!Box by dialing #96*5*! You need to set the password of the FRITZ!Box
in the configuration dialog to display calls

It could be a communication issue, just try again. display calls display phonebook done done, using last list encrypted finishing home login login ok login verification mailboxes active mobile not encrypted preparing quit save and quit show as list show each call show nothing work Project-Id-Version: enigma 2-plugins
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 13:11+0200
PO-Revision-Date: 2009-06-04 20:22+0200
Last-Translator: José Juan Zapater <josej@zapater.fdns.net>
Language-Team: none
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Sobre FritzCall Añadir entrada a la agenda Todos Todas llamadas Añadir número corto Añadir tipo de número Añadir nombre de vanidad Austria Añadir el Llamante automáticamente a la Agenda Llamar Monitorizar llamada Cancelar Conectado desde Conectado a FRITZ!Box! Conectando a FRITZ!Box
(%s)
reintentando... Conectando a FRITZ!Box... Se perdió la conexión a FRITZ!Box!
(%s)
reintentando... No puedo analizar las entradas de la agenda de FRITZ!Box País Teléfonos DECT registrados Testear Borrar Borrar entrada Visualizar llamadas FRITZ!Box-Fon en la pantalla Visualizar todas llamadas Visualizar llamadas entrantes Visualizar llamadas perdidas Visualizar llamadas salientes ¿Qué hago? ¿Quiere borrar la entrada para

%(number)s

%(name)s? Editar Editar entrada seleccionada Introduzca Términos de Búsqueda Entrada incompleta. Número de extensión para iniciar la llamada Dirección FRITZ!Box FON (Nombre o IP) Estado de Fon FRITZ!Box Flash Francia Configuración FritzCall Alemania Consiguiendo llamadas desde FRITZ!Box... Consiguiendo estado desde Fon FRITZ!Box... Dirección IP: Entrada Llamadas entrantes Italia Últimas 10 llamadas:
 Buscar MSN a mostrar (separado por ,) Carpeta de correo Perdidas Llamadas perdidas Silencio en llamada Nombre Nuevo No hay teléfono DECT registrado No hay entrada seleccionada No cuenta de correo activa No hay resultado desde LDIF No hay resultado desde la exportación de Outlook No hay resultado de resolución inversa Número OK Un teléfono DECT registrado Una cuenta de correo activa Salida Llamadas salientes Contraseña accediendo a FRITZ!Box Llamadas de teléfono Ubicación de la Agenda Agenda Salir Leer Agenda desde FRITZ!Box Resetear Búsqueda inversa del ID llamante (seleccionar país abajo) Búsqueda inversa... Guardar Buscar Buscar (may/min indistinto) Buscar agenda Buscando en LDIF... Buscando en la exportación de Outlook... Acceso rápido Mostrar Llamadas Salientes Mostrar después del inicio Mostrar detalles de la entrada Quitar el 0 al inicio Suiza Holanda Tiempo cumplido para Notificaciones de Llamadas (segundos) Marcar Cuenta de Correo Marcar WLAN DESCONOCIDO Usar Agenda interna Vanidad ¡Necesitas activar la monitorización en tu FRITZ!Box marcando #96*5*! Necesita poner la contraseña del FRITZ!Box
en el diálogo de configuración para visualizar las llamadas

Puede hacer un problema de comunicación, sólo reintente otra vez. visualizar llamadas visualizar agenda hecho hecho, usando la última lista encriptado terminando casa identificar identificación ok verificación de entrada cuentas activas móvil no encriptado preparando salir guardar y salir mostrar como lista mostrar cada llamada no mostrar nada trabajo 