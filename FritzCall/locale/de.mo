��    �      �  �   �      �     �     �     �  	   �     �     �       3        R  )   Z     �     �     �     �  
   �     �     �  .   �     #     1     A  0   Y     �  /   �  '   �  )   �  2   '     Z     b     p     �     �     �  %   �     �     �     �     	          5  =   >     |     �     �     �     �     �  $   �  $     (   8     a  '   �  $   �     �      �  2     !   A     c  '   �  $   �  "   �     �          '  N   C  #   �     �     �  <   �               *     2  $   R     w  #   �     �  �   �     [     j     p     �     �     �     �     �     �     �     �     �     �     �     �          1     C     U     b     v     �     �     �     �     �     �            �        �     �     �     �  	             '     ,     J     Y  &   g     �  /   �     �     �     �     �     �          %     D     M     a     t      �     �     �     �     �     �       (        ?     Q     c     u     �     �     �     �     �  
   �     �     �       F     �   _     �     �          #     1     C     H  	   ^  	   h     r     w     }     �     �     �     �     �     �     �     �     �        	          *      /      =      J      Y      f      k   ]  �      �!     �!     "     "     "     ."     F"  3   ]"     �"  /   �"     �"     �"     �"     �"  
   #     #  ,   #  U   B#     �#     �#     �#  >   �#     $  8   '$  ;   `$  3   �$  G   �$     %     %     *%     G%     M%     V%  (   g%     �%     �%     �%     �%     �%     �%  F    &     G&     O&     b&     o&  #   �&  "   �&     �&  3   �&  8   '  &   S'  5   z'  1   �'  "   �'      (  9   &(  !   `(  (   �(  @   �(  4   �(  ,   !)     N)     m)  #   �)  Q   �)  &   �)  #   *     C*  Y   I*  
   �*     �*     �*  *   �*      �*     +     *+  	   J+  �   T+     ,     ,  $   ,     A,  $   H,     m,     ~,     �,     �,     �,     �,     �,     �,     �,      �,     	-     "-     ;-     W-     h-      -  "   �-     �-     �-     �-     �-     .      .     '.  �   0.     �.     �.     /     /     0/     </     S/  "   [/     ~/     �/  +   �/     �/  (   �/     �/  	   
0     0  %   0     @0     Q0     b0     }0     �0     �0     �0  #   �0     �0     
1     1     71     M1     U1     a1     z1     �1     �1     �1     �1  	   2     2  	   2  	   !2  
   +2     62     S2     o2  P   v2  �   �2  	   h3     r3     �3     �3     �3     �3     �3     �3     �3     �3     �3     4     4     4     64     <4     I4     b4     u4     �4     �4     �4     �4     �4     �4     �4     5     "5     05  )   65     �   �                �           �   _      .   y       &       A   2   "       �   �       $       �             R   M             Z      �       3              k       -   p   4   O   �   +      Y       �   �   �       t   *   0   /   �   e      [   �      ]   c   %   >   �   n          �   (   �   �   J   L   �      m   P               �   F   u   5   �   h   �           �           �   �      `       <       9   b           C   �       s      ,   
       @           z   o   �   B       6   v       ?       ^   �   Q   �   i   �              w   �           S   �   a          T      �   �   |       \   �   �   q   �      �   !      ~              8   �   {   �           }   I   g       �      G       	   ;   H   7       =   l          �   �           :   �   '   X   r              �   x          �   �   �   �       V   #   �   j   �           K   f   D   �   �   �           )   �   1         �   �       W   �   E   d   �               U   N                About FritzCall Add entry to phonebook All All calls Append shortcut number Append type of number Append vanity name Areacode to add to calls without one (if necessary) Austria Automatically add new Caller to PhoneBook Call Call monitoring Call redirection Call redirection active Calling %s Cancel Cannot get calls from FRITZ!Box Cannot get infos from FRITZ!Box yet; try later Compact Flash Connected since Connected to FRITZ!Box! Connecting to FRITZ!Box failed
 (%s)
retrying... Connecting to FRITZ!Box... Connection to FRITZ!Box! lost
 (%s)
retrying... Could not get call list; wrong version? Could not parse FRITZ!Box Phonebook entry Could not read FRITZ!Box phonebook; wrong version? Country DECT inactive DECT phones registered Debug Delete Delete entry Display FRITZ!box-Fon calls on screen Display all calls Display connection infos Display incoming calls Display missed calls Display outgoing calls Do what? Do you really want to delete entry for

%(number)s

%(name)s? Edit Edit selected entry Enter Search Terms Entry incomplete. Extended Search for faces Extended Search for names Extension number to initiate call on FRITZ!Box - Could not load calls: %s FRITZ!Box - Could not load phonebook: %s FRITZ!Box - Dialling failed: %s FRITZ!Box - Error getting blacklist: %s FRITZ!Box - Error getting status: %s FRITZ!Box - Error logging in

 FRITZ!Box - Error logging in: %s FRITZ!Box - Error logging in: %s
Disabling plugin. FRITZ!Box - Error logging out: %s FRITZ!Box - Error resetting: %s FRITZ!Box - Failed changing Mailbox: %s FRITZ!Box - Failed changing WLAN: %s FRITZ!Box FON address (Name or IP) FRITZ!Box FON firmware version FRITZ!Box Fon Status FRITZ!Box PhoneBook to read FRITZ!Box firmware version not configured! Please set it in the configuration. FRITZ!Box not available for calling Filter also list of calls Flash Found picture

%s

But did not load. Probably not PNG, 8-bit France FritzCall Setup Germany Getting calls from FRITZ!Box... Getting status from FRITZ!Box Fon... IP Address: Ignore callers with no phone number Incoming Incoming Call on %(date)s at %(time)s from
---------------------------------------------
%(number)s
%(caller)s
---------------------------------------------
to: %(phone)s Incoming calls Italy Last 10 calls:
 Lookup MSN to show (separated by ,) Mailbox Media directory Missed Missed calls Mute on call Name Network mount New New call No DECT phone registered No call redirection active No entry selected No mailbox active No phonebook No result from LDIF No result from Outlook export No result from reverse lookup Number OK One DECT phone registered One call redirection active One mailbox active Others Outgoing Outgoing Call on %(date)s at %(time)s to
---------------------------------------------
%(number)s
%(caller)s
---------------------------------------------
from: %(phone)s Outgoing calls Password Accessing FRITZ!Box Phone calls PhoneBook Location Phonebook Plugin not active Quit Read PhoneBook from FRITZ!Box Refresh status Refreshing... Reload interval for phonebooks (hours) Reset Reverse Lookup Caller ID (select country below) Reverse searching... Save Search Search (case insensitive) Search phonebook Searching in LDIF... Searching in Outlook export... Shortcut Show Outgoing Calls Show after Standby Show details of entry Show only calls for specific MSN Software fax active Software fax inactive Status not available Strip Leading 0 Switzerland The Netherlands Timeout for Call Notifications (seconds) Toggle 1. mailbox Toggle 2. mailbox Toggle 3. mailbox Toggle 4. mailbox Toggle 5. mailbox Toggle Mailbox Toggle WLAN Toggle all mailboxes UNKNOWN USB Device Use internal PhoneBook User name Accessing FRITZ!Box Vanity You need to enable the monitoring on your FRITZ!Box by dialing #96*5*! You need to set the password of the FRITZ!Box
in the configuration dialog to display calls

It could be a communication issue, just try again. before 05.27 call redirections active devices active display calls display phonebook done done, using last list encrypted finishing home login login ok login verification mailboxes active mobile no calls no device active not configured not encrypted not yet implemented number suppressed one device active preparing quit save and quit show as list show each call show nothing work wrong user or password? Project-Id-Version: Enigma2 FritzCall Plugin
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 13:11+0200
PO-Revision-Date: 2013-04-07 13:12+0200
Last-Translator: Michael Schmidt <michael@schmidt-schmitten.com>
Language-Team: german <de@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 FritzCall Plugin Neuer Telefonbucheintrag Alle Alle Anrufe Füge Kurzwahl an Füge Typ der Nummer an Füge Vanity-Nummer an Vorwahl für Anrufe ohne eine solche (falls nötig) Österreich Anrufer automatisch dem Telefonbuch hinzufügen Anrufen Anrufanzeige Rufumleitung Rufumleitung aktiv Rufe %s an Abbruch Kann Anrufe nicht von der FRITZ!Box bekommen Kann Informationen von der FRITZ!Box noch nciht bekommen; versuche es später nochmal Compact Flash Verbunden seit Verbunden mit FRITZ!Box! Verbindung zur FRITZ!Box fehlgeschlagen
 (%s)
neuer Versuch... Verbinde mit FRITZ!Box... Verbindung mit FRITZ!Box verloren
 (%s)
neuer Versuch... Konnte Anrufliste nicht bekommen; falsche Firmware-Version? Konnte Eintrag in FRITZ!Box-Telefonbuch nicht lesen Konnte Telefonbuch der FRITZ!Box nicht lesen; falsche Firmware-Version? Land DECT inaktiv Schnurlostelefone angemeldet Debug Löschen Eintrag löschen Anzeige der Anrufe auf der FRITZ!Box Fon Alle Anrufe Anzeige von Verbindungsinfos Eingehende Anrufe Verpasste Anrufe Abgehende Anrufe Aktion mit Nummer? Soll der Eintrag

%(name)s

für %(number)s wirklich gelöscht werden? Ändern Eintrag bearbeiten Suchbegriffe Eintrag unvollständig. Erweiterte Suche für Anruferbilder Erweiterte Suche für Anrufernamen Nebenstelle für Anrufe FRITZ!Box - Anrufe konnten nicht geladen werden: %s "FRITZ!Box - Telefonbuch konnte nicht geladen werden: %s FRITZ!Box - Wählen fehlgeschlagen: %s FRITZ!Box - Sperrliste konnte nicht geholt werden: %s FRITZ!Box - Status konnte nicht geholt werden: %s FRITZ!Box - Fehler bei Anmeldung

 FRITZ!Box - Fehler bei Login: %s FRITZ!Box - Fehler bei Login: %s
Plugin wird abgeschaltet FRITZ!Box - Fehler bei Logout: %s FRITZ!Box - Fehler bei Zurücksetzen: %s FRITZ!Box - Fehler bei Änderung des Anrufbeantworter-Status: %s FRITZ!Box - Fehler bei Änderung des WLAN-Status: %s FRITZ!Box FON Adresse (Name oder IP-Adresse) FRITZ!Box FON Firmware-Version FRITZ!Box Fon Status Einzulesendes FRITZ!Box Telefonbuch FRITZ!Box Firmware-Version nicht konfiguriert! Bitte in der Konfiguration setzen. FRITZ!Box für Anrufe nicht verfügbar Filter auch auf Anrufliste anwenden Flash Anruferbild gefunden:

%s

Konnte aber nicht geladen werden.
Womöglich nicht PNG, 8-bit? Frankreich FritzCall Einstellungen Deutschland Hole Liste der Anrufe von der FRITZ!Box... Hole Status von der FRITZ!Box... IP Adresse: Ignoriere Anrufe ohne Rufnummer Eingehend Eingehender Anruf am %(date)s um %(time)s von
---------------------------------------------
%(number)s
%(caller)s
---------------------------------------------
an: %(phone)s Eingehende Anrufe Italien Die letzten zehn Anrufe im Standby:
 Suchen anzuzeigende MSNs (getrennt durch ,) Anrufbeantworter Media Verzeichnis Verpasst Verpasste Anrufe Ton aus bei angezeigten Anrufen Name Netzwerk Neu Neuer Anruf Kein Schnurlostelefon angemeldet Keine Rufumleitung aktiv Kein Eintrag ausgewählt Kein Anrufbeantworter aktiv Kein Telefonbuch Kein Ergebnis von LDIF Kein Ergebnis von Outlook-Export Kein Ergebnis von Rückwärtssuche Nummer OK Ein Schnurlostelefon angemeldet Eine Rufumleitung aktiv Ein Anrufbeantworter aktiv Andere Abgehend Abgehender Anruf am %(date)s um %(time)s an
---------------------------------------------
%(number)s
%(caller)s
---------------------------------------------
von: %(phone)s Abgehende Anrufe Passwort der FRITZ!Box Telefonanrufe Speicherort des Telefonbuchs Telefonbuch Plugin ist nicht aktiv Beenden Telefonbuch der FRITZ!Box auslesen Status neu holen Lade neu... Lade Telefonbücher neu nach soviel Stunden Reset Rückwärtssuche (bitte Land auswählen) Rückwärtssuche... Speichern Suche Suche Zeichenfolge (groß/klein egal) Telefonbuchsuche Suche in LDIF... Suche in Outlook-Export... Kurzwahl Zeige abgehende Anrufe an Anzeige nach Standby Details des Eintrags anzeigen Zeige nur Anrufe bestimmter Nummern Software Fax aktiv Software Fax inaktiv Status nicht verfügbar Führende 0 entfernen Schweiz Niederlande Anzeigedauer in Sekunden Toggle 1. Anrufbeantworter Toggle 2. Anrufbeantworter Toggle 3. Anrufbeantworter Toggle 4. Anrufbeantworter Toggle 5. Anrufbeantworter Toggle AB Toggle WLAN Toggle AB UNBEKANNT USB Gerät Benutze internes Telefonbuch Benutzername für FRITZ!Box Vanity Monitoring auf der FRITZ!Box muss durch Wählen von #96*5* eingeschaltet werden! In der Konfiguration muss das Passwort für
die FRITZ!Box gesetzt sein.

Es könnte eine Kommunikationsproblem mit der FRITZ!Box sein.
Versuchen Sie es nochmal. vor 05.27 Rufumleitungen aktiv WLAN-Stationen aktiv Alle Anrufe Telefonbuch fertig fertig, benutze letzte Liste verschlüsselt Fertigstellung Privat Login Login OK Login Verifikation Anrufbeantworter aktiv Handy keine Anrufe keine WLAN-Station aktiv nicht konfiguriert nicht verschlüsselt noch nicht eingebaut Nummer unterdrückt eine WLAN-Station aktiv Vorbereitung Beenden Sichern und Beenden Liste der Anrufe Anzeige der einzelnen Anrufe keine Anzeige Büro falscher Benutzer oder falsches Passwort? 