��    ~        �   �      �
     �
     �
     �
  	   �
     �
     �
          &  )   .     X     ]     m     t     �  0   �     �  /   �  )        B     J     a     g     n  %   {     �     �     �     �     �  =   �     =     B     V     i  $   {      �  "   �     �     �     �                 $   >     c     o     x     �     �     �     �     �     �     �     �     �     �     �               0     D     b     �     �     �     �     �     �     �     �     �  	                  8  /   >     n     �     �     �     �     �     �     �     �               4     D     P  (   `     �     �     �     �     �     �     �     �               2  F   9  �   �               /     4  	   J  	   T     ^     c     i     r     �     �     �  	   �     �     �     �     �     �     �  X  �     O     ^     z     �     �     �     �  
   �  8   �            	   ,     6     F  @   ]      �  :   �  ;   �     6     ;     Y     _  	   f  ,   p     �  "   �     �     �       B   %     h     q     �     �  '   �  $   �     �          '  	   -     7  	   G  '   Q  &   y  	   �     �     �     �     �     �  )   �          $     +     >     O     T      Z     {     �     �  !   �  #   �                     0     D     M  !   b     �     �     �     �     �     �  :   �     #     6     >     E     e          �     �     �     �  "   �          '  	   3  +   =     i     |     �     �     �     �     �     �     �      �       R   %  �   x          +     =  #   C  	   g     q     �     �     �     �     �     �     �     �     �     �     �           #      3      |   Q   7       X   S      L         W              3             !   b   	   {      A         H   4   P       i               c   >   C       ,   E   "          d   r      (   p   \   6   K   h   N   -   D       %      f   ^       q          [      j       u         Z               5   l   :   n      U   R   /       `   G   x           z   .   @   V      1          e               o   $   _       #       Y      k   m   +          ]   ?      O       }   J       *           a   0   B       
   w           =           ~      '       I       v      &   <   T                    g   s   t       8   F               y       2   9   ;                  M   )    About FritzCall Add entry to phonebook All All calls Append shortcut number Append type of number Append vanity name Austria Automatically add new Caller to PhoneBook Call Call monitoring Cancel Connected since Connected to FRITZ!Box! Connecting to FRITZ!Box failed
 (%s)
retrying... Connecting to FRITZ!Box... Connection to FRITZ!Box! lost
 (%s)
retrying... Could not parse FRITZ!Box Phonebook entry Country DECT phones registered Debug Delete Delete entry Display FRITZ!box-Fon calls on screen Display all calls Display incoming calls Display missed calls Display outgoing calls Do what? Do you really want to delete entry for

%(number)s

%(name)s? Edit Edit selected entry Enter Search Terms Entry incomplete. Extension number to initiate call on FRITZ!Box - Error logging in: %s FRITZ!Box FON address (Name or IP) FRITZ!Box Fon Status Flash France FritzCall Setup Germany Getting calls from FRITZ!Box... Getting status from FRITZ!Box Fon... IP Address: Incoming Incoming calls Italy Last 10 calls:
 Lookup MSN to show (separated by ,) Mailbox Missed Missed calls Mute on call Name New No DECT phone registered No entry selected No mailbox active No result from LDIF No result from Outlook export No result from reverse lookup Number OK One DECT phone registered One mailbox active Outgoing Outgoing calls Password Accessing FRITZ!Box Phone calls PhoneBook Location Phonebook Quit Read PhoneBook from FRITZ!Box Reset Reverse Lookup Caller ID (select country below) Reverse searching... Save Search Search (case insensitive) Search phonebook Searching in LDIF... Searching in Outlook export... Shortcut Show Outgoing Calls Show after Standby Show details of entry Strip Leading 0 Switzerland The Netherlands Timeout for Call Notifications (seconds) Toggle 1. mailbox Toggle 2. mailbox Toggle 3. mailbox Toggle 4. mailbox Toggle 5. mailbox Toggle Mailbox Toggle WLAN Toggle all mailboxes UNKNOWN Use internal PhoneBook Vanity You need to enable the monitoring on your FRITZ!Box by dialing #96*5*! You need to set the password of the FRITZ!Box
in the configuration dialog to display calls

It could be a communication issue, just try again. display calls display phonebook done done, using last list encrypted finishing home login login ok login verification mailboxes active mobile not encrypted preparing quit save and quit show as list show each call show nothing work Project-Id-Version: nl
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 13:11+0200
PO-Revision-Date: 2009-06-04 20:19+0200
Last-Translator: 
Language-Team:  <en@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.11.4
Plural-Forms: nplurals=2; plural=(n != 1);
 Over FritzCall Aan telefoonlijst toevoegen Alles Alle oproepen Snelkiesnummer toevoegen Nummersoort toevoegen Alias toevoegen Oostenrijk Nieuw contact automatisch toevoegen aan het telefoonboek Gesprek Gespreksmonitoring Annuleren Verbonden sinds Verbonden met Fritzbox Verbinding naar de FRITZ!Box geweigerd
 (%s)
probeert opnieuw... Aan het verbinden met  FRITZ!Box Verbinding met  FRITZ!Box verbroken
 (%s)
probeert opnieuw Fout bij het verwerken van een FRITZ!Box telefoonboek regel Land geregistreerde DECT telefoons Debug Wissen Wis regel Laat FRITZ!Box gesprekken op het scherm zien Laat alle gesprekken zien Laat binnenkomende gesprekken zien Laat gemiste gesprekken zien Laat uitgaande gesprekken zien Wat te doen... Wilt u echt onderstaande regel verwijderen ?

%(number)s

%(name)s Bewerken Gesel. regel bewerken Geef de zoekterm in Regel incompleet Voorkies nummer voor een telefoonoproep FRITZ!Box Login mislukt! - Error: %s FRITZ!Box adres (Naam of IP) FRITZ!Box Status Flash Frankrijk Setup Fritzcall Duitsland Ontvangt gesprekken van de FRITZ!Box... Ophalen van status van de FRITZ!Box... IP Adres: Binnenkomend Inkomende gesprekken Italië Laatste 10 gesprekken: 
 Zoek Laat MSN zien (door een komma gescheiden) Mailbox Gemist Gemiste gesprekken Muten bij oproep Naam Nieuw Geen DECT telefoon geregistreerd Geen regel geselecteerd Geen mailbox actief Geen resultaat uit LDIF Geen Resultaat van Outlook export Geen resultaat uit omgekeerd zoeken Nummer Ok Eén DECT telefoon geregistreerd Eén mailbox actief Uitgaand Uitgaande gesprekken Wachtwoord voor toegang FRITZ!Box Telefoongesprekken Telefoonboek locatie Telefoonboek Afbreken Lees telefoonboek van FRITZ!Box Reset Omgekeerd zoeken van een beller ID (select land hieronder) Zoekt omgekeerd... Opslaan Zoeken Zoeken (hoofdletter ongevoelig) Doorzoek het telefoonboek Zoekt in LDIF... Zoekt in Outlook export... Snelkoppeling Laat uitgaande gesprekken zien Laat zien na standby Laat de details zien van een regel Verwijder beginnende 0 Zwitzerland Nederland Timeout voor gespreksnotificatie (seconden) 1. Mailbox Aan/Uit 2. Mailbox Aan/Uit 3. Mailbox Aan/Uit 4. Mailbox Aan/Uit 5. Mailbox Aan/Uit Mailbox Aan/Uit WLAN Aan/Uit Mailbox Aan/Uit ONBEKEND Gebruik het interne telefoonboek Trots U moet de monitorfunctie van de FRITZ!Box inschakelen door het drukken van #96*5*! U moet het wachtwoord van de FRITZ!Box nog aanmaken
in het configuratiescherm om gesprekken te laten zien

Dit kan een communicatieprobleem zijn, probeer opnieuw. Toon gesprekken Toon telefoonboek klaar klaar, gebruikt nu de laatste lijst gecodeerd wordt beëindigd home login login ok login verificatie mailboxen actief mobiel ongecodeerd  aan het voorbereiden sluiten opslaan en sluiten als lijst tonen alle gesprekken tonen niets weergeven werk 