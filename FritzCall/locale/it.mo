��    �      �  �   <      �     �     	        	   $     .     E     [  3   n     �  )   �     �     �     �  
   �               ,     :     J  0   b     �  /   �  '   �  )     2   0     c     k     �     �     �  %   �     �     �     �               0  =   9     w     |     �     �  $   �  $   �  (   �     (  '   H  $   p     �      �  2   �  !        *  '   J  $   r  "   �     �     �     �  N   
  #   Y     }     �  <   �     �     �     �     �  $        >  #   J     n  �   w     "     1     7     G     N     k     s     �     �     �     �     �     �     �     �     �     �     
          )     =     [     y     �     �     �     �     �     �  �   �     �     �     �     �  	   �     �     �     �       &         G  /   M     }     �     �     �     �     �     �     �               -      C     d     x     �     �     �  (   �     �     �               *     <     K     W     l  
   t          �     �  F   �  �        �     �     �     �     �     �     �  	     	                        )     <     M     T     ]     n     }     �     �     �  	   �     �     �     �     �     �     	  :       I!     [!     v!     |!     �!  (   �!     �!  >   �!     '"  &   /"     V"     ]"     s"     �"     �"  -   �"     �"     �"     �"  D   �"  &   <#  B   c#  6   �#  =   �#  =   $     Y$     a$     z$     �$     �$  %   �$     �$  !   �$     �$     %     %  	   *%  )   4%     ^%     c%     y%     �%  1   �%  0   �%  /   	&  $   9&  ,   ^&  (   �&     �&  (   �&  G   �&  (   A'     j'  4   �'  (   �'  %   �'  !   (     0(     F(  G   d(  )   �(  #   �(     �(  O    )     P)     X)     q)  *   z)  .   �)     �)  *   �)     *  �   *     �*     �*     �*     �*      �*     +     .+  	   >+     H+     W+     h+     m+     {+     �+     �+  "   �+     �+     �+     ,  "   ,  -   ?,  3   m,     �,     �,     �,     �,     �,     -  	   -  �   -     �-  "   �-  
   �-     .     .     ".     4.  #   ;.     _.  '   s.     �.  0   �.  #   �.     �.     �.  %   /     ,/     ?/     R/     p/     }/  !   �/     �/  +   �/     0     0     +0     C0     L0  #   S0     w0     �0     �0     �0     �0     �0     �0     �0      1     1     1  #   51     Y1  >   b1  �   �1     G2     Y2     t2     �2     �2     �2  "   �2  
   �2     �2     �2     �2     �2     3     3  	   ,3     63     >3     X3     h3      w3     �3     �3     �3     �3     �3     �3     �3      4     4        \   r   `   o   )          �   �           u       f   G   �   #   	       �   I   2       k       3   .   �   a          �           �   (                      �   �   A       t   �   ]   �      �   �   �      �   /   �           �   F       ^          E   �   g   %       �      H   9      �       j   n   �   e       �   �   &   V       D              U          �      �   6   $   K   B   *       �       4   �   �       1   �   �           �   z       {   +   s      �   w      q   �      
       |   �   �          �   5   ?   S   _   7          b   ~           i   �   h          �       !       0   �          "   �   v   8   x      '              [   }   C   @      �   �   �       �   �          p   =          d       M       �   W              -          ,   �   c       N              O   Q   <   :       �   ;          T       P   l   L   y   �       �   R           �   m   J   Y   Z      X       >        About FritzCall Add entry to phonebook All All calls Append shortcut number Append type of number Append vanity name Areacode to add to calls without one (if necessary) Austria Automatically add new Caller to PhoneBook Call Call monitoring Call redirection Calling %s Cancel Cannot get calls from FRITZ!Box Compact Flash Connected since Connected to FRITZ!Box! Connecting to FRITZ!Box failed
 (%s)
retrying... Connecting to FRITZ!Box... Connection to FRITZ!Box! lost
 (%s)
retrying... Could not get call list; wrong version? Could not parse FRITZ!Box Phonebook entry Could not read FRITZ!Box phonebook; wrong version? Country DECT phones registered Debug Delete Delete entry Display FRITZ!box-Fon calls on screen Display all calls Display connection infos Display incoming calls Display missed calls Display outgoing calls Do what? Do you really want to delete entry for

%(number)s

%(name)s? Edit Edit selected entry Enter Search Terms Entry incomplete. Extension number to initiate call on FRITZ!Box - Could not load calls: %s FRITZ!Box - Could not load phonebook: %s FRITZ!Box - Dialling failed: %s FRITZ!Box - Error getting blacklist: %s FRITZ!Box - Error getting status: %s FRITZ!Box - Error logging in

 FRITZ!Box - Error logging in: %s FRITZ!Box - Error logging in: %s
Disabling plugin. FRITZ!Box - Error logging out: %s FRITZ!Box - Error resetting: %s FRITZ!Box - Failed changing Mailbox: %s FRITZ!Box - Failed changing WLAN: %s FRITZ!Box FON address (Name or IP) FRITZ!Box FON firmware version FRITZ!Box Fon Status FRITZ!Box PhoneBook to read FRITZ!Box firmware version not configured! Please set it in the configuration. FRITZ!Box not available for calling Filter also list of calls Flash Found picture

%s

But did not load. Probably not PNG, 8-bit France FritzCall Setup Germany Getting calls from FRITZ!Box... Getting status from FRITZ!Box Fon... IP Address: Ignore callers with no phone number Incoming Incoming Call on %(date)s at %(time)s from
---------------------------------------------
%(number)s
%(caller)s
---------------------------------------------
to: %(phone)s Incoming calls Italy Last 10 calls:
 Lookup MSN to show (separated by ,) Mailbox Media directory Missed Missed calls Mute on call Name Network mount New New call No DECT phone registered No call redirection active No entry selected No mailbox active No phonebook No result from LDIF No result from Outlook export No result from reverse lookup Number OK One DECT phone registered One call redirection active One mailbox active Others Outgoing Outgoing Call on %(date)s at %(time)s to
---------------------------------------------
%(number)s
%(caller)s
---------------------------------------------
from: %(phone)s Outgoing calls Password Accessing FRITZ!Box Phone calls PhoneBook Location Phonebook Plugin not active Quit Read PhoneBook from FRITZ!Box Refresh status Reload interval for phonebooks (hours) Reset Reverse Lookup Caller ID (select country below) Reverse searching... Save Search Search (case insensitive) Search phonebook Searching in LDIF... Searching in Outlook export... Shortcut Show Outgoing Calls Show after Standby Show details of entry Show only calls for specific MSN Software fax active Status not available Strip Leading 0 Switzerland The Netherlands Timeout for Call Notifications (seconds) Toggle 1. mailbox Toggle 2. mailbox Toggle 3. mailbox Toggle 4. mailbox Toggle 5. mailbox Toggle Mailbox Toggle WLAN Toggle all mailboxes UNKNOWN USB Device Use internal PhoneBook User name Accessing FRITZ!Box Vanity You need to enable the monitoring on your FRITZ!Box by dialing #96*5*! You need to set the password of the FRITZ!Box
in the configuration dialog to display calls

It could be a communication issue, just try again. before 05.27 call redirections active devices active display calls display phonebook done done, using last list encrypted finishing home login login ok login verification mailboxes active mobile no calls no device active not configured not encrypted not yet implemented number suppressed one device active preparing quit save and quit show as list show each call show nothing work Project-Id-Version: enigma2  - FRITZ!Box
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-07 13:11+0200
PO-Revision-Date: 2013-01-20 15:17+0100
Last-Translator: Spaeleus <spaeleus@croci.org>
Language-Team: www.linsat.net <spaeleus@croci.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Language: Italian
X-Poedit-Country: ITALY
X-Poedit-SourceCharset: utf-8
X-Poedit-Basepath: .../enigma2-plugins/fritzcall
X-Poedit-SearchPath-0: .../enigma2-plugins/fritzcall
 Info su FritzCall Aggiungere voce in rubrica Tutte Tutte le chiamate Aggiungere numero breve Agg. il tipo di numero (casa,cell.,uff.) Aggiungere nome "Vanity" Pref. locale per le chiamate che ne sono prive (se necessario) Austria Ins. autom. nuovo chiamante in rubrica Chiam. Monitoraggio chiamate Deviazione chiamate Chiamata per %s Annull. Recupero chiamate dalla FRITZ!Box impossibile work Connesso da Connesso alla FRITZ!Box! Connessione alla FRITZ!Box fallita!
(%s)
Nuovo tentativo in corso... Connessione alla FRITZ!Box in corso... Connessione alla FRITZ!Box persa!
(%s)
Nuovo tentativo in corso... Impossibile ottenere elenco chiamate: versione errata? Impossibile analizzare la voce nella Rubrica della FRITZ!Box! Impossibile leggere Rubrica della FRITZ!Box: versione errata? Nazione Telefoni DECT registrati Debug Cancell. Cancellare voce Visualizzare chiamate telef. sulla TV Tutte le chiamate Mostrare informazioni connessione Chiamate in ingresso Chiamate perse Chiamate in uscita Che fare? Cancellare la voce

%(number)s

%(name)s? Mod. Mod. voce selezionata Inserire criteri di ricerca Voce incompleta. Numero apparecchio su cui indirizzare la chiamata FRITZ!Box - Impossibile caricare le chiamate: %s FRITZ!Box - Impossibile caricare la rubrica: %s FRITZ!Box - Composizione fallita: %s FRITZ!Box - Errore in recupero blacklist: %s FRITZ!Box - Errore in recupero stato: %s FRITZ!Box - Errore log in

 FRITZ!Box - Errore in esecuzione log: %s FRITZ!Box - Errore in esecuzione log: %s
Il plugin verrà disabilitato. FRITZ!Box - Errore in esecuzione log: %s FRITZ!Box - Errore in reset: %s FRITZ!Box - Cambiamento casella di posta fallito: %s FRITZ!Box - Cambiamento WLAN fallito: %s FRITZ!Box FON - Indirizzo (nome o IP) FRITZ!Box FON - Versione firmware FRITZ!Box Fon - Stato Rubrica FRITZ!Box da caricare Versione firmware FRITZ!Box non inserita. Aggiornare la configurazione. FRITZ!Box non disponibile per le chiamate Filtrare anche gli elenchi chiamate Flash Trovata immagine

%s

Impossibile caricarla. Il formato deve esserecpng, 8 bit. Francia Configurazione FritzCall Germania Recupero chiamate dalla FRITZ!Box in corso Recupero stato dalla FRITZ!Box Fon in corso... Indirizzo IP: Ignorare i chiamanti con numero mascherato In Entr. Chiamata in arrivo il %(date)s alle %(time)s da
---------------------------------------------
%(number)s
%(caller)s
---------------------------------------------
a: %(phone)s Chiamate in entrata Italia Ultime 10 chiamate:
 Lookup MSN da mostrare (separare con ,) Casella di posta Directory media Ch. perse Chiamate perse Mute su Chiamata Nome Mount di rete Nuovo Nuova chiamata Nessun telefono DECT registrato Nessuna deviazione chiamate attiva Nessuna voce selezionata Nessuna casella di posta attiva Nessuna rubrica Ricerca in LDIF: nessun risultato! Ricerca in Outlook  export: nessun risultato! Ricerca identificativo chiamante: nessun risultato! Numero Ok Un telefono DECT registrato Una deviazione chiamate attiva Una casella di posta attiva Altri paesi In uscita Chiamata in uscita il %(date)s alle %(time)s a
---------------------------------------------
%(number)s
%(caller)s
---------------------------------------------
da: %(phone)s Chiamate in uscita Password di accesso alla FRITZ!Box Ch. telef. Posizione rubrica Rubrica Plugin non attivo Uscire Caricare la rubrica dalla FRITZ!Box Aggiornare lo stato Intervallo aggiornamento rubriche (ore) Reset Identificativo chiamante (selezionare il Paese!) Ricerca identificativo chiamante... Salvare Ricerca Ricerca (non distingue Maiusc-minusc) Ricerca su rubrica Ricerca in LDIF... Ricerca in Outlook  export... Numero breve Mostrare le chiamate in uscita Mostrare chiamate dopo lo standby Mostrare dettagli voce Mostrare solo le chiamate per MSN specifico Fax software attivo Stato non disponibile Sopprimere "0" iniziali Svizzera Olanda Ritardo notifica chiamate (secondi) Comm. casella 1. Comm. casella 2. Comm. casella 3. Comm. casella 4. Comm. casella 5. Comm. casella Commutare WLAN Comm. tutte le caselle SCONOSCIUTO Dispositivo USB Usare la rubrica interna User name di accesso alla FRITZ!Box "Vanity" Per abilitare il monitoraggio sulla FRITZ!Box comporre #96*5*! E' necessario impostare la password della FRITZ!Box
nel menu configurazione per mostrare le chiamate.

Potrebbe trattarsi di un problema di comunicazione, riprovare. anteriore a 05.27 deviazione chiamate attiva Dispositivi attivi Tutte le chiamate Mostrare rubrica Fatto Fatto, sarà usata l'ultimo elenco codificato Quasi terminato... Casa Login Login Ok Verifica login caselle di posta attive Cellulare Nessuna Nessun dispositivo attivo non configurato non codificato Funzione non ancora implementata numero mascherato Un dispositivo attivo Preparazione in corso Uscire Salvare e uscire Come elenco Tutte Nulla Ufficio 