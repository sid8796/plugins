# -*- coding: iso-8859-15 -*-
from Plugins.Plugin import PluginDescriptor
from enigma import evfd, eTimer, eDVBVolumecontrol, eServiceCenter, iPlayableService, iServiceInformation
from Screens.Screen import Screen
from Components.ServiceEventTracker import ServiceEventTracker
from Components.ActionMap import ActionMap
from Components.Label import Label
from Components.config import config, ConfigSlider, ConfigSubsection, getConfigListEntry, ConfigSelection, configfile, ConfigElement
from Components.ConfigList import ConfigListScreen
from RecordTimer import RecordTimer

# Icons:
# 1 = PlayFastBackward, 2 = PlayBackward, 3 = Play, 4 = PlayForward, 5 = PlayFastForward, 6 = Pause, 7 = REC_1, 8 = Mute, 9 = Loop
#10 = Dolby, 11 = ConditionalAccess(CA), 12 = CommonInterface(CI), 13 = USB, 14 = Subchannels(DoubleScreen), 15 = REC_2, 16 = HDD_8, 17 = HDD_7, 18 = HDD_6, 19 = HDD_5
#20 = HDD_4, 21 = HDD_3, 22 = HDD_FULL, 23 = HDD_2, 24 = HDD_1, 25 = MP3, 26 = AC3, 27 = HDTV(Screen), 28 = Radio(Note), 29 = Alert
#30 = HDD_0, 31 = PM, 32 = AM, 33 = Timer(Clock), 34 = Clock(not to apply), 35 = RemoteControl(not to apply), 36 = Standby(not to apply), 37 = Terrestrial, 38 = Disc_3, 39 = Disc_2
#40 = Disc_1, 41 = Disc_0, 42 = Satellite, 43 = TimeShift, 44 = RecordTimeShift(Dot), 45 = Cable, 46 = All(all icons simultaneously)

config.plugins.vfdicon = ConfigSubsection()
config.plugins.vfdicon.dispcontent = ConfigSelection(default = 'name', choices = [
		('name', _('Kanal-Name oder Titel')), ('number_name', _('Kanal-Nummer-Name oder Titel')), ('number', _('Kanal-Nummer oder Titel')), ('off', _('keine')) ])
config.plugins.vfdicon.hddicons = ConfigSelection(default = 'off', choices = [('on', _('ja')), ('off', _('nein'))])
config.plugins.vfdicon.stbyhddicons = ConfigSelection(default = 'off', choices = [('on', _('ja')), ('off', _('nein'))])
config.plugins.vfdicon.hddcarrier = ConfigSelection(default = 'usb', choices = [
		('share', _('Netzwerk Festplatte')), ('internal', _('Nand')), ('rec', _('REC Partition')), ('usb', _('USB Festplatte')) ])
config.plugins.vfdicon.stbyusbicon = ConfigSelection(default = 'off', choices = [('on', _('ja')), ('off', _('nein'))])
config.plugins.vfdicon.animestart = ConfigSelection(default = 'no', choices = [('yes', _('ja')), ('no', _('nein'))])
config.plugins.vfdicon.animeactivate = ConfigSelection(default = 'no', choices = [('yes', _('ja')), ('no', _('nein'))])
config.plugins.vfdicon.animeactivity = ConfigSelection(default = 'record', choices = [
		('play', _('nur Wiedergabe')), ('tshift', _('nur TimeShift')), ('all', _('alle Festplattenaktivitäten')), ('record', _('nur Aufnahme')) ])
config.plugins.vfdicon.stbyanime = ConfigSelection(default = 'off', choices = [('on', _('ja')), ('off', _('keine'))])
config.plugins.vfdicon.systimesync = ConfigSelection(default = 'no', choices = [('ntp2.fau.de', _('GPS')), ('ntp3.fau.de', _('DCF77 Signal')), ('no', _('nein'))])
config.plugins.vfdicon.brightness = ConfigSlider(default=2, limits=(0, 3))
config.plugins.vfdicon.stbybright = ConfigSlider(default=0, limits=(0, 3))

class config_VFD(Screen, ConfigListScreen):
	skin = """
<screen name="config_VFD" position="center,center" size="540,355" title="VFD Einstellungen">
	<widget name="key_red" position="5,3" zPosition="1" size="130,30" valign="center" halign="center" font="Regular;22" transparent="1" />
	<widget name="key_green" position="135,3" zPosition="1" size="130,30" valign="center" halign="center" font="Regular;22" transparent="1" />
	<widget name="key_yellow" position="270,3" zPosition="1" size="130,30" valign="center" halign="center" font="Regular;20" transparent="1" />
	<widget name="key_blue" position="405,3" zPosition="1" size="130,30" valign="center" halign="center" font="Regular;20" transparent="1" />
	<ePixmap position="5,35" zPosition="2" size="130,3" pixmap="/usr/lib/enigma2/python/Plugins/SystemPlugins/VFD-Icons/images/red.png" transparent="1" alphatest="on" />
	<ePixmap position="135,35" zPosition="2" size="130,3" pixmap="/usr/lib/enigma2/python/Plugins/SystemPlugins/VFD-Icons/images/green.png" transparent="1" alphatest="on" />
	<ePixmap position="270,35" zPosition="2" size="130,3" pixmap="/usr/lib/enigma2/python/Plugins/SystemPlugins/VFD-Icons/images/yellow.png" transparent="1" alphatest="on" />
	<ePixmap position="405,35" zPosition="2" size="130,3" pixmap="/usr/lib/enigma2/python/Plugins/SystemPlugins/VFD-Icons/images/blue.png" transparent="1" alphatest="on" />
	<widget name="config" position="15,50" zPosition="3" size="510,300" scrollbarMode="showOnDemand" />
</screen>"""

	def __init__(self, session):
		self.session = session
		Screen.__init__(self, session)
		self.cfglist = []
		self.cfglist.append(getConfigListEntry(_('Schriftanzeige:'),
			config.plugins.vfdicon.dispcontent))
		self.cfglist.append(getConfigListEntry(_('HDD Symbole anzeigen:'),
			config.plugins.vfdicon.hddicons))
		self.cfglist.append(getConfigListEntry(_('HDD Symbole in Standby anzeigen:'),
			config.plugins.vfdicon.stbyhddicons))
		self.cfglist.append(getConfigListEntry(_('HDD Symbole - Datenträger:'),
			config.plugins.vfdicon.hddcarrier))
		self.cfglist.append(getConfigListEntry(_('USB Symbol in Standby anzeigen:'),
			config.plugins.vfdicon.stbyusbicon))
		self.cfglist.append(getConfigListEntry(_('Disc Animation bei Start aktivieren:'),
			config.plugins.vfdicon.animestart))
		self.cfglist.append(getConfigListEntry(_('Disc Animation bis Neustart aktivieren:'),
			config.plugins.vfdicon.animeactivate))
		self.cfglist.append(getConfigListEntry(_('Disc Animation - Aktivität:'),
			config.plugins.vfdicon.animeactivity))
		self.cfglist.append(getConfigListEntry(_('Disc Animation bei Aufnahme in Standby:'),
			config.plugins.vfdicon.stbyanime))
		self.cfglist.append(getConfigListEntry(_('Mit Internetzeitserver synchronisieren:'),
			config.plugins.vfdicon.systimesync))
		self.cfglist.append(getConfigListEntry(_('Standard Helligkeit:'),
			config.plugins.vfdicon.brightness))
		self.cfglist.append(getConfigListEntry(_('Standby Helligkeit:'),
			config.plugins.vfdicon.stbybright))
		ConfigListScreen.__init__(self, self.cfglist)
		self['actions'] = ActionMap(['SetupActions', 'ColorActions'],
			{
				'cancel': self.cancel,
				'red': self.cancel,
				'ok': self.ok,
				'green': self.ok,
				'yellow': self.yellow,
				'blue': self.blue,
			}, -2)
		self['key_red'] = Label(_('Abbrechen'))
		self['key_green'] = Label(_('OK'))
		self['key_yellow'] = Label(_('Symbole aus'))
		self['key_blue'] = Label(_('Schrift aus'))
		self['config'].setList(self.cfglist)

	def cancel(self):
		for x in self['config'].list:
			x[1].cancel()
		self.close()

	def ok(self):
		global VFD_icons_instance
		VFD_icons_instance = VFD_icons(self.session)
		evfd.getInstance().vfd_set_brightness(config.plugins.vfdicon.brightness.value)
		if config.plugins.vfdicon.dispcontent.value == 'off':
			VFD_icons_instance.service_name = None
			evfd.getInstance().vfd_clear_string()
		else:
			VFD_icons_instance.service_name = None
			VFD_icons_instance.display_service_name()
		if config.plugins.vfdicon.hddicons.value == 'on':
			VFD_icons_instance.mount = None
			VFD_icons_instance.HDD_icons()
			VFD_icons_instance.HDD = True
		else:
			VFD_icons_instance.HDD = False
			VFD_icons_instance.clear_HDD_icons()
			VFD_icons_instance.mount = None
		if config.plugins.vfdicon.animeactivate.value == 'yes':
			config.plugins.vfdicon.animestart.value = 'no'
			VFD_icons_instance.anime_timer.startLongTimer(1)
		elif config.plugins.vfdicon.animestart.value == 'no':
			VFD_icons_instance.REC_anime = False
			VFD_icons_instance.PLAY_anime = False
			VFD_icons_instance.anime_timer.stop()
		if config.plugins.vfdicon.systimesync.value != 'no':
			config.plugins.vfdicon.systimesync.value = 'no'
			from os import system
			system('/sbin/rdate -s %s > /dev/null' % config.plugins.vfdicon.systimesync.value)
		for x in self['config'].list:
			x[1].save()
		configfile.save()
		self.close()

	def yellow(self):
		evfd.getInstance().vfd_clear_icons()

	def blue(self):
		evfd.getInstance().vfd_clear_string()

def open_config(session, **kwargs):
	session.open(config_VFD)

def VFD(menuid, **kwargs):
	if menuid == 'system':
		return [(_('VFD Display'), open_config, 'vfd_display', 44)]
	else:
		return []

class VFD_icons:
	def __init__(self, session):
		self.session = session
		self.inquiry_timer = eTimer()
		self.inquiry_timer.callback.append(self.inquiry)
		self.frequent_timer = eTimer()
		self.frequent_timer.callback.append(self.frequent_inquiry)
		self.anime_timer = eTimer()
		self.anime_timer.callback.append(self.anime)
		self.onClose = [ ]
		self.tune_fail = False
		self.in_standby = False
		self.USB = False
		self.mount = None
		self.HDD = False
		self.HDD_space = -1
		self.SAT = self.CAB = self.TER = False
		self.TV = True
		self.CA = 0
		self.S_CHANNEL = False
		self.AC3 = self.DD = self.MP3 = False
		self.service_name = None
		self.HDTV = False
		self.mute = False
		self.PLAY = self.TSHIFT = self.REC_TSHIFT = False
		self.REC_timer = False
		self.REC = 0
		self.PLAY_anime = self.REC_anime = False
		self.disc = 0
		self.__event_tracker = ServiceEventTracker(screen=self,eventmap=
			{
				iPlayableService.evTunedIn: self.on_tune_occur,
				iPlayableService.evTuneFailed: self.on_tune_failure,
				iPlayableService.evUpdatedEventInfo: self.service_icons,
				iPlayableService.evNewProgramInfo: self.service_icons,
				iPlayableService.evUpdatedInfo: self.display_service_name,
				iPlayableService.evVideoSizeChanged: self.HDTV_icon,
				iPlayableService.evSeekableStatusChanged: self.PLAY_icons,
				iPlayableService.evStart: self.PLAY_icons,
			})
		config.misc.standbyCounter.addNotifier(self.on_enter_standby, initial_call = False)
		session.nav.record_event.append(self.REC_icons)

	def on_tune_occur(self):
		if self.tune_fail:
			self.tune_fail = False
			self.service_name = None
			self.icon_off(29)
			evfd.getInstance().vfd_clear_string()

	def on_tune_failure(self):
		self.tune_fail = True
		self.icon_on(29)
		evfd.getInstance().vfd_write_string('NICHT EMPFANGBAR')
		self.clear_service_icons()

	def on_enter_standby(self, configElement):
		from Screens.Standby import inStandby
		inStandby.onClose.append(self.on_leave_standby)
		self.in_standby = True
		evfd.getInstance().vfd_set_brightness(config.plugins.vfdicon.stbybright.value)
		self.clear_service_icons()
		if self.mute:
			self.icon_off(8)
		if self.HDD and config.plugins.vfdicon.stbyhddicons.value == 'off':
			self.HDD = False
			self.clear_HDD_icons()
		elif not self.HDD and config.plugins.vfdicon.stbyhddicons.value == 'on':
			self.mount = None
			self.HDD_icons()
			self.HDD = True
		if self.USB and config.plugins.vfdicon.stbyusbicon.value == 'off':
			self.icon_off(13)

	def on_leave_standby(self):
		self.in_standby = False
		evfd.getInstance().vfd_set_brightness(config.plugins.vfdicon.brightness.value)
		self.service_name = None
		if self.mute:
			self.icon_on(8)
		if not self.HDD and config.plugins.vfdicon.hddicons.value == 'on':
			self.HDD_icons()
			self.HDD = True
		elif self.HDD and config.plugins.vfdicon.hddicons.value == 'off':
			self.HDD = False
			self.clear_HDD_icons()
			self.mount = None
		if self.USB and config.plugins.vfdicon.stbyusbicon.value == 'off':
			self.icon_on(13)
		if config.plugins.vfdicon.stbyanime.value == 'on' and config.plugins.vfdicon.animestart.value == 'no' and config.plugins.vfdicon.animeactivate.value == 'no':
			self.anime_timer.stop()

	def inquiry(self):
		if not self.in_standby or config.plugins.vfdicon.stbyusbicon.value == 'on':
			self.USB_icon()
		if self.HDD:
			self.HDD_icons()
		self.inquiry_timer.startLongTimer(60)

	def frequent_inquiry(self):
		if not self.in_standby:
			self.mute_icon()
		self.REC_timer_icon()
		self.frequent_timer.startLongTimer(10)

	def anime(self):
		if self.REC_anime or self.PLAY_anime:
			if self.disc > 4:
				self.disc = 2
			else:
				self.disc = self.disc + 1
			self.disc_icons()
		elif self.disc != 0:
			self.disc = 0
			self.disc_icons()
		self.anime_timer.startLongTimer(1)

	def service_icons(self):
		SAT = CAB = TER = False
		TV = True
		CA = 0
		sub_number = 0
		AC3 = DD = MP3 = False
		service = self.session.nav.getCurrentlyPlayingServiceReference()
		if service:
			tp_data = service.getData(0)
			if tp_data == 2 or tp_data == 10:
				TV = False
		service = self.session.nav.getCurrentService().info()
		if service:
			CA = service.getInfo(iServiceInformation.sIsCrypted) # 0 or 1
			tp_data = service.getInfoObject(iServiceInformation.sTransponderData)
			if tp_data:
				if tp_data['tuner_type'] == 'DVB-S':
					SAT = True
				elif tp_data['tuner_type'] == 'DVB-C':
					CAB = True
				elif tp_data['tuner_type'] == 'DVB-T':
					TER = True
		service = self.session.nav.getCurrentService().subServices()
		if service:
			sub_number = service.getNumberOfSubservices() # 0 or 1 and more
		service = self.session.nav.getCurrentService().audioTracks()
		if service:
			tracks = service.getNumberOfTracks()
			for x in range(tracks):
				track = service.getTrackInfo(x)
				if 'MPEG' in track.getDescription():
					continue
				elif 'AC3' in track.getDescription():
					AC3 = True
					if '5' in track.getLanguage():
						DD = True
				elif 'MP3' in track.getDescription():
					MP3 = True
		if SAT != self.SAT:
			self.SAT = SAT
			if SAT:
				self.icon_on(42)
			else:
				self.icon_off(42)
		if CAB != self.CAB:
			self.CAB = CAB
			if CAB:
				self.icon_on(45)
			else:
				self.icon_off(45)
		if TER != self.TER:
			self.TER = TER
			if TER:
				self.icon_on(37)
			else:
				self.icon_off(37)
		if TV != self.TV:
			self.TV = TV
			if TV:
				self.icon_off(28)
			else:
				self.icon_on(28)
			self.HDTV_icon()
		if CA != self.CA:
			self.CA = CA
			if CA == 1:
				self.icon_on(11)
			else:
				self.icon_off(11)
		if sub_number > 0:
			if not self.S_CHANNEL:
				self.S_CHANNEL = True
				self.icon_on(14)
		elif self.S_CHANNEL:
			self.S_CHANNEL = False
			self.icon_off(14)
		if AC3 != self.AC3:
			self.AC3 = AC3
			if AC3:
				self.icon_on(26)
			else:
				self.icon_off(26)
		if DD != self.DD:
			self.DD = DD
			if DD:
				self.icon_on(10)
			else:
				self.icon_off(10)
		if MP3 != self.MP3:
			self.MP3 = MP3
			if MP3:
				self.icon_on(25)
			else:
				self.icon_off(25)

	def display_service_name(self):
		if config.plugins.vfdicon.dispcontent.value == 'off':
			pass
		else:
			service_name = channel_number = None
			service = self.session.nav.getCurrentService().info()
			if service and self.MP3:
				service_name = service.getInfoString(iServiceInformation.sTagTitle)
			else:
				service = self.session.nav.getCurrentlyPlayingServiceReference()
				if service:
					subservice = service.toString().split('::')
					if subservice[0].count(':') == 9:
						service_name = subservice[1]
					else:
						if config.plugins.vfdicon.dispcontent.value == 'number' or config.plugins.vfdicon.dispcontent.value == 'number_name':
							if self.PLAY and not self.TSHIFT:
								pass
							else:
								channel_number = service.getChannelNum()
						if config.plugins.vfdicon.dispcontent.value != 'number':
							service_handler = eServiceCenter.getInstance()
							if service_handler:
								service_name = service_handler.info(service).getName(service)
			if config.plugins.vfdicon.dispcontent.value == 'number' and not self.MP3 and channel_number:
				service_name = str(channel_number)
			elif service_name: # remove non-displayable characters or character-elements ;)
				service_name = service_name.replace('\x27', ',').replace('\x2b', '*').replace('\x5f', '.').replace('\xc2\x86', '').replace('\xc2\x87', '').replace('\xc3\x84', 'A').replace('\xc3\x96', 'O').replace('\xc3\x9c', 'U').replace('\xc3\x9f', 'ss').replace('\xc3\xa4', 'a').replace('\xc3\xb6', 'o').replace('\xc3\xbc', 'u')
				service_name = service_name.replace('\xc3\x93', 'O').replace('\xc3\xb3', 'o').replace('\xc4\x85', 'a').replace('\xc4\x86', 'C').replace('\xc4\x87', 'c').replace('\xc4\x99', 'e').replace('\xc5\x81', 'L').replace('\xc5\x82', 'l').replace('\xc5\x84', 'n').replace('\xc5\x9a', 'S').replace('\xc5\x9b', 's').replace('\xc5\xb9', 'Z').replace('\xc5\xba', 'z').replace('\xc5\xbb', 'Z').replace('\xc5\xbc', 'z')
				if config.plugins.vfdicon.dispcontent.value == 'number_name' and not self.MP3 and channel_number:
					service_name = str(channel_number) + ' ' + service_name
			if service_name != self.service_name:
				self.service_name = service_name
				service_name = str(service_name).ljust(9)
				evfd.getInstance().vfd_write_string(service_name)

	def HDTV_icon(self):
		video_height = 0
		if not self.TV:
			pass
		else:
			service = self.session.nav.getCurrentService().info()
			if service:
				video_height = service.getInfo(iServiceInformation.sVideoHeight)
		if video_height > 576:
			if not self.HDTV:
				self.HDTV = True
				self.icon_on(27)
		elif self.HDTV:
			self.HDTV = False
			self.icon_off(27)

	def mute_icon(self): # currently must be in timer (frequent_inquiry)
		mute = eDVBVolumecontrol.getInstance().isMuted() # False or True
		if mute != self.mute:
			self.mute = mute
			if mute:
				self.icon_on(8)
			else:
				self.icon_off(8)

	def REC_timer_icon(self): # currently must be in timer (frequent_inquiry)
		waiting = False
		for timer in self.session.nav.RecordTimer.timer_list:
			if timer.state == timer.StateWaiting:
				waiting = True
				break
		if waiting != self.REC_timer:
			self.REC_timer = waiting
			if waiting:
				self.icon_on(33)
			else:
				self.icon_off(33)

	def REC_icons(self, service, event):
		REC = self.session.nav.getRecordings()
		REC_number = len(REC)
		if REC_number == 2 and self.REC == 1:
			self.icon_on(15)
		elif REC_number == 1:
			if self.REC == 0:
				self.icon_on(7)
			else:
				self.icon_off(15)
		elif REC_number == 0 and self.REC > 0:
			self.icon_off(7)
		self.REC = REC_number
		if REC_number > 0:
			self.REC_anime = False
			if config.plugins.vfdicon.animestart.value == 'yes' or config.plugins.vfdicon.animeactivate.value == 'yes':
				if not self.in_standby and (config.plugins.vfdicon.animeactivity.value == 'record' or config.plugins.vfdicon.animeactivity.value == 'all'):
					self.REC_anime = True
			elif self.in_standby and config.plugins.vfdicon.stbyanime.value == 'on':
				self.REC_anime = True
				if config.plugins.vfdicon.animestart.value == 'no' and config.plugins.vfdicon.animeactivate.value == 'no':
					self.anime_timer.startLongTimer(1)
			if self.TSHIFT and not self.REC_TSHIFT:
				self.REC_TSHIFT = True
				self.icon_on(44)
		else:
			if self.REC_anime:
				self.REC_anime = False
			if self.REC_TSHIFT:
				self.REC_TSHIFT = False
				self.icon_off(44)

	def PLAY_icons(self):
		play = None
		service = self.session.nav.getCurrentService().seek()
		if service:
			play = service.isCurrentlySeekable() # 0 or 3
		if play:
			self.PLAY = True
			self.icon_on(2)
			self.icon_on(3)
			self.icon_on(4)
			self.icon_on(6)
		else: # player doesn't put the state back after stoping playing, only after exiting the player
			self.PLAY = False
			self.icon_off(2)
			self.icon_off(3)
			self.icon_off(4)
			self.icon_off(6)
		self.TSHIFT_icons()

	def TSHIFT_icons(self):
		tshift = None
		REC_TSHIFT = False
		service = self.session.nav.getCurrentService().timeshift()
		if service:
			tshift = service.isTimeshiftActive() # 0 or 1
			if tshift and self.REC > 0:
				REC_TSHIFT = True
		if tshift:
			self.TSHIFT = True
			self.icon_on(43)
		else:
			self.TSHIFT = False
			self.icon_off(43)
		if REC_TSHIFT != self.REC_TSHIFT:
			if REC_TSHIFT:
				self.icon_on(44)
			else:
				self.icon_off(44)
		if config.plugins.vfdicon.animestart.value == 'yes' or config.plugins.vfdicon.animeactivate.value == 'yes':
			self.PLAY_anime = False
			if self.PLAY and not self.TSHIFT and config.plugins.vfdicon.animeactivity.value == 'play':
				self.PLAY_anime = True
			elif self.TSHIFT and config.plugins.vfdicon.animeactivity.value == 'tshift':
				self.PLAY_anime = True
			elif (self.PLAY or self.TSHIFT) and config.plugins.vfdicon.animeactivity.value == 'all':
				self.PLAY_anime = True

	def disc_icons(self):
		if self.disc < 2:
			if self.disc == 0:
				self.icon_off(38)
				self.icon_off(39)
				self.icon_off(40)
				self.icon_off(41)
			else:
				self.icon_on(41)
				self.icon_on(38)
				self.icon_on(40)
				self.icon_on(39)
		elif self.disc == 2:
			self.icon_off(38)
		elif self.disc == 3:
			self.icon_off(40)
			self.icon_off(39)
			self.icon_on(38)
		elif self.disc == 4:
			self.icon_on(40)
		else:
			self.icon_on(39)

	def USB_icon(self): # for sdb only
		USB = False
		diskstats = open('/proc/diskstats', 'r')
		for line in diskstats:
			result = line.strip().split()
			if result[2] == 'sdb':
				USB = True
		diskstats.close()
		if USB != self.USB:
			self.USB = USB
			if USB:
				self.icon_on(13)
			else:
				self.icon_off(13)
				if config.plugins.vfdicon.hddcarrier.value == 'usb':
					self.mount = None

	def HDD_icons(self):
		if config.plugins.vfdicon.hddcarrier.value == 'usb':
			self.mount = self.get_mount('/media/sdb1')
		elif config.plugins.vfdicon.hddcarrier.value == 'share':
			self.mount = self.get_mount('/media/share')
		elif config.plugins.vfdicon.hddcarrier.value == 'internal':
			self.mount = self.get_mount('/media/nand')
		else:
			self.mount = self.get_mount('/media/sda4')
		if self.mount:
			from os import statvfs
			carrier = statvfs(self.mount)
			if carrier:
				space = (carrier.f_blocks - carrier.f_bavail)*9/carrier.f_blocks
				if space != self.HDD_space:
					if space >= 0:
						if self.HDD_space < 0:
							self.icon_on(30)
					if space >= 1:
						if self.HDD_space < 1:
							self.icon_on(24)
					else:
						self.icon_off(24)
					if space >= 2:
						if self.HDD_space < 2:
							self.icon_on(23)
					else:
						self.icon_off(23)
					if space >= 3:
						if self.HDD_space < 3:
							self.icon_on(21)
					else:
						self.icon_off(21)
					if space >= 4:
						if self.HDD_space < 4:
							self.icon_on(20)
					else:
						self.icon_off(20)
					if space >= 5:
						if self.HDD_space < 5:
							self.icon_on(19)
					else:
						self.icon_off(19)
					if space >= 6:
						if self.HDD_space < 6:
							self.icon_on(18)
					else:
						self.icon_off(18)
					if space >= 7:
						if self.HDD_space < 7:
							self.icon_on(17)
					else:
						self.icon_off(17)
					if space >= 8:
						if self.HDD_space < 8:
							self.icon_on(16)
					else:
						self.icon_off(16)
					if space >= 9: # 9 = 90%, 10 = 100%
						if self.HDD_space < 9:
							self.icon_on(22)
					else:
						self.icon_off(22)
					self.HDD_space = space
		elif self.HDD_space >= 0:
			self.clear_HDD_icons()

	def get_mount(self, dev):
		mounts = open('/proc/mounts', 'r')
		for line in mounts:
			result = line.strip().split()
			if result[1].startswith(dev):
				mounts.close()
				return result[1]
		mounts.close()
		return None

	def clear_service_icons(self):
		if not self.tune_fail:
			if self.SAT:
				self.SAT = False
				self.icon_off(42)
			if self.CAB:
				self.CAB = False
				self.icon_off(45)
			if self.TER:
				self.TER = False
				self.icon_off(37)
			if not self.TV:
				self.TV = True
				self.icon_off(28)
		if self.HDTV:
			self.HDTV = False
			self.icon_off(27)
		if self.CA == 1:
			self.CA = 0
			self.icon_off(11)
		if self.S_CHANNEL:
			self.S_CHANNEL = False
			self.icon_off(14)
		if self.AC3:
			self.AC3 = False
			self.icon_off(26)
		if self.DD:
			self.DD = False
			self.icon_off(10)
		if self.MP3:
			self.MP3 = False
			self.icon_off(25)

	def clear_HDD_icons(self):
		self.HDD_space = -1
		self.icon_off(22)
		self.icon_off(16)
		self.icon_off(17)
		self.icon_off(18)
		self.icon_off(19)
		self.icon_off(20)
		self.icon_off(21)
		self.icon_off(23)
		self.icon_off(24)
		self.icon_off(30)

	def icon_on(self, num):
		evfd.getInstance().vfd_set_icon(num,1)

	def icon_off(self, num):
		evfd.getInstance().vfd_set_icon(num,0)

VFD_icons_instance = None

def main(session, **kwargs):
	global VFD_icons_instance
	if VFD_icons_instance is None:
		VFD_icons_instance = VFD_icons(session)
	evfd.getInstance().vfd_set_brightness(config.plugins.vfdicon.brightness.value)
	VFD_icons_instance.inquiry()
	VFD_icons_instance.frequent_inquiry()
	if config.plugins.vfdicon.hddicons.value == 'on':
		VFD_icons_instance.HDD_icons()
		VFD_icons_instance.HDD = True
	if config.plugins.vfdicon.animeactivate.value == 'yes':
		config.plugins.vfdicon.animeactivate.value = 'no'
	if config.plugins.vfdicon.animestart.value == 'yes':
		VFD_icons_instance.anime()
	if config.plugins.vfdicon.dispcontent.value == 'off':
		evfd.getInstance().vfd_clear_string()

def Plugins(**kwargs):
	return [
	PluginDescriptor(name = _('VFD Display'),
		description = _('VFD display config 15-10-2014'), where = PluginDescriptor.WHERE_MENU,
		fnc = VFD),
	PluginDescriptor(where = PluginDescriptor.WHERE_SESSIONSTART, fnc = main )]