from Components.Language import language
from Tools.Directories import resolveFilename, SCOPE_PLUGINS

from gettext import bindtextdomain, dgettext, gettext
from os import environ


def localeInit():
	environ["LANGUAGE"] = language.getLanguage()[:2]
	bindtextdomain("PermanentClock", resolveFilename(SCOPE_PLUGINS, \
		"Extensions/PermanentClock/locale"))

localeInit()
language.addCallback(localeInit)
