##
## Permanent Clock
## by AliAbdul, mod by Ducktrick
##
from . import _
from Components.ActionMap import ActionMap
from Components.ConfigList import ConfigList, ConfigListScreen
from Components.config import *
from Components.Language import language
from Components.MenuList import MenuList
from Screens.MessageBox import MessageBox
from enigma import ePoint, eTimer, getDesktop
from os import environ
from Plugins.Plugin import PluginDescriptor
from Screens.Screen import Screen
from Tools.Directories import resolveFilename, SCOPE_LANGUAGE
import gettext
from enigma import eTimer
from Components.Input import Input
from Screens.InputBox import InputBox

##############################################################################

SKIN = """
	<screen position="0,0" size="60,24" zPosition="10" title="Permanent Clock" flags="wfNoBorder">
		<widget source="global.CurrentTime" render="Label" position="0,0" size="60,24" font="Regular;18" valign="center" halign="center" transparent="1">
			<convert type="ClockToText">Default</convert>
		</widget>
	</screen>"""

##############################################################################

config.plugins.PermanentClock = ConfigSubsection()
config.plugins.PermanentClock.enabled = ConfigYesNo(default=True)
config.plugins.PermanentClock.automove = ConfigYesNo(default=True)
config.plugins.PermanentClock.position_x = ConfigInteger(default=590)
config.plugins.PermanentClock.position_y_stable = ConfigInteger(default=590)
config.plugins.PermanentClock.position_y_motion = ConfigInteger(default=590)
config.plugins.PermanentClock.position_y = ConfigInteger(default=35)
config.plugins.PermanentClock.move = ConfigInteger(default=10)
config.plugins.PermanentClock.position_y_state = ConfigInteger(default=0)

##############################################################################

lang = language.getLanguage()
environ["LANGUAGE"] = lang[:2]
gettext.bindtextdomain("enigma2", resolveFilename(SCOPE_LANGUAGE))
gettext.textdomain("enigma2")
gettext.bindtextdomain("PermanentClock", resolveFilename(SCOPE_LANGUAGE))

def _(txt):
	t = gettext.dgettext("PermanentClock", txt)
	if t == txt:
		t = gettext.gettext(txt)
	return t

##############################################################################

class TitleScreen(Screen):
	def __init__(self, session, parent=None):
		Screen.__init__(self, session, parent)
		self.onLayoutFinish.append(self.setScreenTitle)

	def setScreenTitle(self):
		self.setTitle(_("Permanent Clock"))

##############################################################################

class PermanentClockScreen(Screen):
	def __init__(self, session):
		Screen.__init__(self, session)
		self.skin = SKIN
		desktop = getDesktop(0)
		self.desktopWidth = desktop.size().width()
		self.desktopHeight = desktop.size().height()
		self.AutoLoadTimer = eTimer()
		self.AutoLoadTimer.timeout.get().append(self.moveClock)
		if config.plugins.PermanentClock.automove.value:
			self.onShow.append(self.moveClock)
		else:
			self.onShow.append(self.movePosition)

	def moveClock(self):
		value = config.plugins.PermanentClock.position_y.value # dynamic value
		reset = config.plugins.PermanentClock.position_y_stable.value # value + 25
		state = config.plugins.PermanentClock.position_y_state.value # up oder down
		motion = config.plugins.PermanentClock.position_y_motion.value # stable default value

		if value == reset:
			config.plugins.PermanentClock.position_y_state.value = "0"
			state = config.plugins.PermanentClock.position_y_state.value
		elif value == motion:
			config.plugins.PermanentClock.position_y_state.value = "1"
			state = config.plugins.PermanentClock.position_y_state.value

		if state == "0":
			value -= 1
		elif state == "1":
			value += 1

		config.plugins.PermanentClock.position_y.value = value
		if self.instance:
			self.instance.move(ePoint(config.plugins.PermanentClock.position_x.value, config.plugins.PermanentClock.position_y.value))
			self.AutoLoadTimer.start(int(10)*1000)

	def movePosition(self):
		if self.instance:
			self.instance.move(ePoint(config.plugins.PermanentClock.position_x.value, config.plugins.PermanentClock.position_y.value))

##############################################################################

class PermanentClock():
	def __init__(self):
		self.dialog = None

	def gotSession(self, session):
		self.dialog = session.instantiateDialog(PermanentClockScreen)
		self.showHide()

	def changeVisibility(self):
		if config.plugins.PermanentClock.enabled.value:
			config.plugins.PermanentClock.enabled.value = False
		else:
			config.plugins.PermanentClock.enabled.value = True
		config.plugins.PermanentClock.enabled.save()
		self.showHide()

	def changeVisibilityAutomove(self):
		if config.plugins.PermanentClock.automove.value:
			config.plugins.PermanentClock.automove.value = False
		else:
			config.plugins.PermanentClock.automove.value = True
		config.plugins.PermanentClock.automove.save()

	def showHide(self):
		if config.plugins.PermanentClock.enabled.value:
			self.dialog.show()
		else:
			self.dialog.hide() 			

pClock = PermanentClock()

##############################################################################

class PermanentClockPositioner(Screen):
	def __init__(self, session):
		Screen.__init__(self, session)
		self.skin = SKIN
		
		self["actions"] = ActionMap(["WizardActions"],
		{
			"left": self.left,
			"up": self.up,
			"right": self.right,
			"down": self.down,
			"ok": self.ok,
			"back": self.exit
		}, -1)
		
		desktop = getDesktop(0)
		self.desktopWidth = desktop.size().width()
		self.desktopHeight = desktop.size().height()
		
		self.moveTimer = eTimer()
		self.moveTimer.timeout.get().append(self.movePosition)
		self.moveTimer.start(50, 1)

	def movePosition(self):
		self.instance.move(ePoint(config.plugins.PermanentClock.position_x.value, config.plugins.PermanentClock.position_y.value))
		self.moveTimer.start(50, 1)

	def left(self):
		value = config.plugins.PermanentClock.position_x.value
		value -= 1
		if value < 0:
			value = 0
		config.plugins.PermanentClock.position_x.value = value

	def up(self):
		value = config.plugins.PermanentClock.position_y.value
		if config.plugins.PermanentClock.automove.value == True:
			movevalue = config.plugins.PermanentClock.move.value
		value -= 1
		if value < 0:
			value = 0
		config.plugins.PermanentClock.position_y.value = value
		if config.plugins.PermanentClock.automove.value == True:
			config.plugins.PermanentClock.position_y_motion.value = value
			config.plugins.PermanentClock.position_y_state.value = 0
			config.plugins.PermanentClock.position_y_stable.value = value + movevalue

	def right(self):
		value = config.plugins.PermanentClock.position_x.value
		value += 1
		if value > self.desktopWidth:
			value = self.desktopWidth
		config.plugins.PermanentClock.position_x.value = value

	def down(self):
		value = config.plugins.PermanentClock.position_y.value
		if config.plugins.PermanentClock.automove.value == True:
			movevalue = config.plugins.PermanentClock.move.value
		value += 1
		if value > self.desktopHeight:
			value = self.desktopHeight
		config.plugins.PermanentClock.position_y.value = value
		if config.plugins.PermanentClock.automove.value == True:
			config.plugins.PermanentClock.position_y_state.value = 0
			config.plugins.PermanentClock.position_y_motion.value = value
			config.plugins.PermanentClock.position_y_stable.value = value + movevalue

	def ok(self):
		config.plugins.PermanentClock.position_x.save()
		config.plugins.PermanentClock.position_y.save()
		if config.plugins.PermanentClock.automove.value == True:
			config.plugins.PermanentClock.position_y_state.save()
			config.plugins.PermanentClock.position_y_stable.save()
			config.plugins.PermanentClock.position_y_motion.save()
		self.close()

	def exit(self):
		config.plugins.PermanentClock.position_x.cancel()
		config.plugins.PermanentClock.position_y.cancel()
		if config.plugins.PermanentClock.automove.value == True:
			config.plugins.PermanentClock.position_y_state.cancel()
			config.plugins.PermanentClock.position_y_stable.cancel()
			config.plugins.PermanentClock.position_y_motion.cancel()
		self.close()

##############################################################################

class PermanentClockMenu(TitleScreen):
	skin = """
		<screen position="150,235" size="420,150" title="Permanent Clock">
			<widget name="list" position="10,10" size="400,120" />
		</screen>"""

	def __init__(self, session):
		TitleScreen.__init__(self, session)
		self.session = session
		self["list"] = MenuList([])
		self["actions"] = ActionMap(["OkCancelActions"], {"ok": self.okClicked, "cancel": self.close}, -1)
		self.onLayoutFinish.append(self.showMenu)

	def showMenu(self):
		list = []
		if config.plugins.PermanentClock.enabled.value:
			list.append(_("Deactivate permanent clock"))
		else:
			list.append(_("Activate permanent clock"))

		if config.plugins.PermanentClock.automove.value:
			list.append(_("Deactivate Automove clock"))
		else:
			list.append(_("Activate Automove clock"))
			
		list.append(_("Change permanent clock position"))

		if config.plugins.PermanentClock.automove.value == True:
			list.append(_("Motion Pixel eingabe"))

		self["list"].setList(list)


	def SetPixelimput(self):
		self.session.openWithCallback(self.askForWord, InputBox, title=_("Motion Pixel angeben z.B. 25"), text=" " * 55, maxSize=55)

	def askForWord(self, word):
		if word is None:
			config.plugins.PermanentClock.move.value = 10
			config.plugins.PermanentClock.move.save()
		else:
			value = int(word)
			config.plugins.PermanentClock.move.value = value
			config.plugins.PermanentClock.move.save()
		pClock.showHide()

	def positionerCallback(self, callback=None):
		pClock.showHide()

	def okClicked(self):
		sel = self["list"].getCurrent()
		if sel == _("Deactivate permanent clock") or sel == _("Activate permanent clock"):
			if pClock.dialog is None:
				pClock.gotSession(self.session)
			pClock.changeVisibility()
			self.showMenu()
		elif sel == _("Deactivate Automove clock") or sel == _("Activate Automove clock"):
			if pClock.dialog is None:
				pClock.gotSession(self.session)
			pClock.changeVisibilityAutomove()
			self.showMenu()
		elif sel == _("Motion Pixel eingabe"):
			self.SetPixelimput()
		else:
			if config.plugins.PermanentClock.automove.value == True:
				if config.plugins.PermanentClock.move.value == " ":
					self.session.open(MessageBox,_("Bitte erst Motion Pixel eintragen !"), MessageBox.TYPE_INFO)
				else:
					if pClock.dialog is None:
						pClock.gotSession(self.session)
					pClock.dialog.hide()
					self.session.openWithCallback(self.positionerCallback, PermanentClockPositioner)
			else:
					if pClock.dialog is None:
						pClock.gotSession(self.session)
					pClock.dialog.hide()
					self.session.openWithCallback(self.positionerCallback, PermanentClockPositioner)

##############################################################################

def sessionstart(reason, **kwargs):
	if reason == 0:
		pClock.gotSession(kwargs["session"])

def main(session, **kwargs):
	session.open(PermanentClockMenu)

##############################################################################

def Plugins(**kwargs):
	return [
		PluginDescriptor(where=[PluginDescriptor.WHERE_SESSIONSTART], fnc=sessionstart),
		PluginDescriptor(name=_("Permanent Clock"), description=_("Shows the clock permanent on the screen"), where=PluginDescriptor.WHERE_PLUGINMENU, fnc=main)]
