# -*- coding: iso-8859-15 -*-
#
#
# Write by Ducktrick
# 	(c)2014
#
from Plugins.Plugin import PluginDescriptor
import ServiceReference
from enigma import iPlayableService, iDVBFrontend, eTimer, eServiceCenter, iServiceInformation, iRecordableService, iFrontendInformation, eServiceReference, eDVBVolumecontrol, evfd
from Screens.Screen import Screen
from Screens.VirtualKeyBoard import VirtualKeyBoard
from Components.ServiceEventTracker import ServiceEventTracker, InfoBarBase
from Components.ActionMap import ActionMap
from Components.Label import Label
from Components.Button import Button
from Components.config import config, ConfigSlider, ConfigSubsection, getConfigListEntry, ConfigSelection, ConfigText, configfile
from Components.ConfigList import ConfigListScreen
from Components.ScrollLabel import ScrollLabel
from Screens.MessageBox import MessageBox

from RecordTimer import RecordTimerEntry, parseEvent, AFTEREVENT
from timer import TimerEntry as RealTimerEntry
from time import localtime, strftime, sleep
from Tools.Directories import fileExists
import datetime
import os

class SystemInfo( Screen ):
	skin = """
		<screen name="SystemInfo" position="760,0" size="520,720" title="SystemInfo" backgroundColor="#002C2C39" flags="wfNoBorder">
			<widget name="Titel" position="0,0" size="520,720" zPosition="1" halign="center" render="Label" font="Regular;24" alphatest="on" />
			<widget name="TunerStatus" position="10,50" size="520,20" zPosition="2" render="Label" font="Regular;20" alphatest="on" />
			<widget name="Channelname" position="50,90" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="FrontendSignal" position="50,110" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="FrontendSignalQuality" position="50,130" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="FrontendSignalPower" position="50,150" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="FrontendStatus" position="50,170" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="FrontendSignalQualitydB" position="50,190" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="FrontendBER" position="50,210" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="FrontendLOCKED" position="50,230" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="Crypt" position="50,250" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="CurrentFrontend" position="50,270" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="Image" position="10,320" size="520,20" zPosition="2" render="Label" font="Regular;20" alphatest="on" />
			<widget name="Version" position="50,350" size="520,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="EMU" position="50,370" size="520,20" zPosition="2" render="Label" font="Regular;18" alphatest="on" />
			<widget name="MHZ" position="50,390" size="520,20" zPosition="2" render="Label" font="Regular;18" alphatest="on" />
			<widget name="RAM" position="50,410" size="520,20" zPosition="2" render="Label" font="Regular;18" alphatest="on" />
			<widget name="SWAP" position="50,430" size="520,20" zPosition="2" render="Label" font="Regular;18" alphatest="on" />
			<widget name="Buffer" position="50,450" size="520,20" zPosition="2" render="Label" font="Regular;18" alphatest="on" />
			<widget name="CPUUSED" position="50,470" size="520,20" zPosition="2" render="Label" font="Regular;18" alphatest="on" />
			<widget name="Size" position="50,490" size="520,20" zPosition="2" render="Label" font="Regular;18" alphatest="on" />
			<widget name="HDDTitel" position="10,520" size="520,20" zPosition="2" render="Label" font="Regular;20" alphatest="on" />
			<widget name="SDA1" position="50,550" size="470,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="SDA2" position="50,570" size="470,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="SDA4" position="50,590" size="470,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="SDB1" position="50,610" size="470,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="share" position="50,630" size="470,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="SDC1" position="50,650" size="470,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<widget name="SDD1" position="50,670" size="470,20" zPosition="2" foregroundColor="white" font="Regular;18"/>
			<ePixmap pixmap="/usr/lib/enigma2/python/Plugins/Extensions/SystemInfo/key_green.png" position="50,690" zPosition="2" size="30,30" transparent="1" alphatest="on" />
			<widget name="key_green" position="85,690" zPosition="3" size="140,30" font="Regular;18" valign="center" halign="center" backgroundColor="#1f771f" transparent="1" />
		</screen>"""
	def __init__ (self, session):
		self.session = session
		Screen.__init__(self, session)
		print "[SystemInfo] __init__\n"
		self.FindTuner0()
		self.FindTuner1()
		if self.TunerType0 == None:
			self.TunerType0 = "N/A"
		if self.TunerType1 == None:
			self.TunerType1 = "N/A"
		self.oscam = False
		self.mgcamd = False
		self.camd3 = False
		self.mbox = False
		self.incubus = False
		self.vizcam = False
		self.cpu = False
		self.ramfree = True
		self.swapfree = True
		self["CurrentFrontend"] = Label()
		self["FrontendSignal"] = Label()
		self["FrontendSignalQuality"] = Label()
		self["FrontendSignalPower"] = Label()
		self["FrontendSignalQualitydB"] = Label()
		self["FrontendLOCKED"] = Label()
		self["FrontendBER"] = Label()
		self["FrontendStatus"] = Label()
		self["Channelname"] = Label()
		#self["TunerArt"] = Label()
		self["Crypt"] = Label()
		self["Size"] = Label()
		self["CPUUSED"] = Label()
		self["MHZ"] = Label()
		self["EMU"] = Label()
		self["RAM"] = Label()
		self["SWAP"] = Label()
		self["Buffer"] = Label()
		self["SDA1"] = Label()
		self["SDA2"] = Label()
		self["SDA4"] = Label()
		self["SDB1"] = Label()
		self["SDC1"] = Label()
		self["SDD1"] = Label()
		self["share"] = Label()
		self["Version"] = Label()
		self["key_green"] = Button(_("Log Speichern"))
		self["Titel"] = Label(_('SystemInformation'))
		self["HDDTitel"] = Label(_('Festplatten :'))
		self["TunerStatus"] = Label(_('Tuner/Kanal Information :'))
		self["Image"] = Label(_('Image Information :'))
		self.AutoLoadTimer = eTimer()
		self.AutoLoadTimer.timeout.get().append(self.SystemInfoStartup)
		self["myActionMap"] = ActionMap(["MinuteInputActions", "MenuActions", "ShortcutActions"],
		{
			"green": self.logfile,
			"cancel": self.cancel
		}, -1)

		self.onLayoutFinish.append(self.SystemInfoStartup)
		
	def SystemInfoStartup(self):
		print "[SystemInfo] Read Sources\n"
		service = self.session.nav.getCurrentService()
		if service:
			info = service.frontendInfo()			
			snrValue = info.getFrontendInfo(iFrontendInformation.snrValue)
			self.msgsnrValue = "Signal : %s " % snrValue
			self["FrontendSignal"].setText(self.msgsnrValue)
			
			signalQuality = info.getFrontendInfo(iFrontendInformation.signalQuality) * 100 / 65536
			self.msgsignalQuality = "Signal Quali. : %s (SNR)" % signalQuality
			self["FrontendSignalQuality"].setText(self.msgsignalQuality)
			
			signalPower = info.getFrontendInfo(iFrontendInformation.signalPower) * 100 / 65536
			self.msgsignalPower = "Signal Power. : %s (AGC)" % signalPower
			self["FrontendSignalPower"].setText(self.msgsignalPower)
			
			signalQualitydB = info.getFrontendInfo(iFrontendInformation.signalQualitydB) / 100.0 / 65536
			self.msgsignalQualitydB = "Signal Quali. dB. : %3.02f (SNR_DB)" % signalQualitydB
			self["FrontendSignalQualitydB"].setText(self.msgsignalQualitydB)
			
			lockState = info.getFrontendInfo(iFrontendInformation.lockState)
			if lockState == 1:
				lockState = "Locked"
			else:
				lockState = "Not Locked"
			self.msglockState = "Lock : %s " % lockState
			self["FrontendLOCKED"].setText(self.msglockState)
			
			bitErrorRate = info.getFrontendInfo(iFrontendInformation.bitErrorRate)
			self.msgbitErrorRate = "BER : %s (BER)" % bitErrorRate
			self["FrontendBER"].setText(self.msgbitErrorRate)
			
			frontendStatus = info.getFrontendInfo(iFrontendInformation.frontendStatus)
			self.msgfrontendStatus = "Frontend Status : %s " % frontendStatus
			self["FrontendStatus"].setText(self.msgfrontendStatus)
			
			self.servicename = self.session.nav.getCurrentlyPlayingServiceReference()
			servicename = self.servicename.toCompareString()
			servicenamecomplete = ServiceReference.ServiceReference(servicename).getServiceName().replace('\xc2\x87', '').replace('\xc2\x86', '').ljust(16)
			self.msgservicenamecomplete = "Aktueller Kanal : %s " % servicenamecomplete
			self["Channelname"].setText(self.msgservicenamecomplete)
			
			# search Frontend
			frontendNumber = info.getFrontendInfo(iFrontendInformation.frontendNumber)
			if frontendNumber == 0:
				TunerInformation = "Tuner(A) - " + self.TunerType0
			if frontendNumber == 1:
				TunerInformation = "Tuner(B) - " + self.TunerType1
			self.msgfrontendNumber = "Verwendetter Tuner : %s" % (TunerInformation)
			self["CurrentFrontend"].setText(self.msgfrontendNumber)
			
			infos = service.info()
			crypted = infos.getInfo(iServiceInformation.sIsCrypted)
			if crypted == 1:
				self.crypt = "Kanal : Verschlüsselt"
			else:
				self.crypt = "Kanal : FreeTV"
			self["Crypt"].setText(self.crypt)
			
			serviceinfo = service.info()
			height = serviceinfo and serviceinfo.getInfo(iServiceInformation.sVideoHeight) or -1
			width = serviceinfo and serviceinfo.getInfo(iServiceInformation.sVideoWidth) or -1
			if height == -1 and width == -1:
				self.size = "Auflösung : N/A"
				self["Size"].setText(self.size)
			else:
				self.size = "Auflösung : %sx%s" % (width, height)
				self["Size"].setText(self.size)

		# find Emu
		if self.oscam == False and self.mgcamd == False and self.camd3 == False and self.mbox == False and self.incubus == False and self.vizcam == False:
			self.findemu()
		if self.oscam:
			 self.OSCamVersion()
		if self.mgcamd:
			self.msgOSV = "Emu/Version : MG-Camd 138"
			self["EMU"].setText(self.msgOSV)
		if self.camd3:
			self.msgOSV = "Emu/Version : Camd3_902"
			self["EMU"].setText(self.msgOSV)
		if self.mbox:
			self.msgOSV = "Emu/Version : MBox_0.6_BETA_0010"
			self["EMU"].setText(self.msgOSV)
		if self.incubus:
			self.msgOSV = "Emu/Version : IncubusCamd_0.99"
			self["EMU"].setText(self.msgOSV)
		if self.vizcam:
			self.msgOSV = "Emu/Version : Vizcam 135"
			self["EMU"].setText(self.msgOSV)
		#find cpu mhz
		if self.cpu == False:
			self.findcpumhz()
		#find free ram
		if self.ramfree == True and self.swapfree == True:
			self.findfreeram()
		# retur Image Version
		self.ImageVersion()
		# return the HDD size Funktion
		self.CheckHDDState()
		self.findcpuauslastung()

		self.AutoLoadTimer.start(int(1)*1000)
		
	def FindTuner0(self):
		try:
			TunerRead0 = open("/proc/Tunertype", 'r')
			for i in TunerRead0:
				if i.find('stb6100') != -1 or i.find('stv6110x') != -1 or i.find('sharp7306') != -1:
					Tuner0 = "DVB-S2"
				if i.find('lg031') != -1:
					Tuner0 = "DVB-C"
				if i.find('sharp6465') != -1:
					Tuner0 = "DVB-T"
			self.TunerType0 = Tuner0
		except:
			self.TunerType0 = None

	def FindTuner1(self):
		try:
			TunerRead1 = open("/proc/Tunertype2", 'r')
			for i in TunerRead1:  
				if i.find('stb6100') != -1 or i.find('stv6110x') != -1 or i.find('sharp7306') != -1:
					Tuner1 = "DVB-S2"
				if i.find('lg031') != -1:
					Tuner1 = "DVB-C"
				if i.find('sharp6465') != -1:
					Tuner1 = "DVB-T"
			self.TunerType1 = Tuner1
		except:
			self.TunerType1 = None

	def findfreeram(self):
		try:
			ram = open('/proc/meminfo', 'r')
			for x in ram:
				if x.find('MemFree:') != -1:
					freeram = x.split('MemFree:')[1].split('kB')[0].strip()
				if x.find('SwapFree:') != -1:
					freeswap = x.split('SwapFree:')[1].split('kB')[0].strip()
				if x.find('Buffers:') != -1:
					usedbuffer = x.split('Buffers:')[1].split('kB')[0].strip()
			self.msgram = "Freier RAM : %s kB" % freeram
			self.msgswap = "Freier SWAP : %s kB" % freeswap
			self.msgbuffer = "Benutzter Mem Buffer : %s kB" % usedbuffer
			self["Buffer"].setText(self.msgbuffer)
			self["RAM"].setText(self.msgram)
			self["SWAP"].setText(self.msgswap)
			self.ramfree = True
			self.swapfree = True
		except:
			self.msgram = "Freier RAM : N/A "
			self.msgswap = "Freier SWAP : N/A "
			self.msgbuffer = "Benutzter Mem Buffer : N/A"
			self["Buffer"].setText(self.msgbuffer)
			self["RAM"].setText(self.msgram)
			self["SWAP"].setText(self.msgswap)
			self.swapfree = False
			self.ramfree = False
		
	def findcpumhz(self):
		try:
			cpu = open('/proc/cpu_frequ/pll0_ndiv_mdiv', 'r')
			for x in cpu:
				if x.find('SYSACLKOUT') != -1:
					MHZ = x.split('(standard 266MHZ) = ')[1]
			self.msgMHZ = "CPU Takt : %s " % MHZ
			self["MHZ"].setText(self.msgMHZ)
			self.cpu = True
		except:
			self.msgMHZ = "CPU Takt : N/A "
			self["MHZ"].setText(self.msgMHZ)
			self.cpu = False
			
	def findcpuauslastung(self):
		try:
			cpulast = open('/proc/cpu_frequ/sysaclkout', 'r')
			for x in cpulast:
				if x.find('measured') != -1:
					BOGOMIPSUSED = int(x.split('(measured)=')[1].strip())
			# usedbogomips : 262 bogomips * 100 = % cpu last
			used = BOGOMIPSUSED * 100 / 262
			self.msgCPUUSED = "CPU Auslastung : %s %%" % used
			self["CPUUSED"].setText(self.msgCPUUSED)
		except:
			self.msgCPUUSED = "CPU Auslastung : N/A"
			self["CPUUSED"].setText(self.msgCPUUSED)

	def findemu(self):
		try:
			os.system('ps > /tmp/bakfile')
			emu = open('/tmp/bakfile', 'r')
			for x in emu:
				if x.find('/var/emu/oscam') != -1:
					self.oscam = True
				if x.find('/var/emu/mgcamd.sh4') != -1:
					self.mgcamd = True
				if x.find('/var/emu/camd3_902') != -1:
					self.camd3 = True
				if x.find('/var/emu/mbox_0.6_BETA_0010') != -1:
					self.mbox = True
				if x.find('/var/emu/incubusCamd_0.99') != -1:
					self.incubus = True
				if x.find('/var/emu/vizcam.sh4') != -1:
					self.vizcam = True
		except:
			pass
		
	def ImageVersion(self):
		# read installed Version
		try:
			f = open('/var/config/update/version', 'r')
			for x in f:
				version = x
			self.msgVersion = "Name/Version : Ducktrick RAMFS V%s " % version
			self["Version"].setText(self.msgVersion)
		except:
			print 'Error no File FOUND'
			self.msgVersion = "Name/Version : N/A"
			self["Version"].setText(self.msgVersion)
			
	def OSCamVersion(self):
		# read installed Version
		try:
			f = open('/var/keys/oscam.conf', 'r')
			for x in f:
				if x.find(' OSCAM ') != -1:
					OSV = x.split(' OSCAM ')[1].replace("_svn","").replace("_emu","").replace("SVN","")
			self.msgOSV = "Emu/Version : OS-Cam %s " % OSV
			self["EMU"].setText(self.msgOSV)
		except:
			print 'Error no File FOUND'
			self.msgOSV = "Emu/Version : N/A"
			self["EMU"].setText(self.msgOSV)
		
	def CheckHDDState(self):
		# find usb devices
		self.sda = False
		self.sdb = False
		self.sdc = False
		self.sdd = False
		self.share = False
		self.devusbsda = None
		self.devusbsdb = None
		self.devusbsdc = None
		self.devusbsdd = None
		self.netshare = None
		self.devusbsda = self.FindUSBDev('sda')
		self.devusbsdb = self.FindUSBDev('sdb')
		self.devusbsdc = self.FindUSBDev('sdc')
		self.devusbsdd = self.FindUSBDev('sdd')
		if not self.devusbsda:
			self.sda = False
			self.msgsda1 = ""
			self.msgsda2 = ""
			self.msgsda4 = ""
		else:
			self.sda = True
		if not self.devusbsdb:
			self.sdb = False
			self.msgsdb1 = ""
		else:
			self.sdb = True
		if not self.devusbsdc:
			self.sdc = False
			self.msgsdc1 = ""
		else:
			self.sdc = True
		if not self.devusbsdd:
			self.sdd = False
			self.msgsdd1 = ""
		else:
			self.sdd = True
		#find share
		FileListfromDir = os.listdir('/media/share/')
		for x in FileListfromDir:
			if x == "":
				pass
			self.netshare = self.FindMountDev('/media/share/' + x)
			if self.netshare is not None:
				self.sharesize = self.CheckSize(self.netshare)
				if self.sharesize > 100000:
					self.sharesize = self.sharesize/1024
					self.GB = True
				else:
					self.GB = False
				self.sharepoint = '/media/share/' + x
				self.share = True
				
		if self.sda:
			self.devusbsda1 = self.FindMountDev('/media/sda1')
			self.devusbsda2 = self.FindMountDev('/media/sda2')
			self.devusbsda4 = self.FindMountDev('/media/sda4')
			self.sda1 = self.CheckSize(self.devusbsda1)
			self.sda2 = self.CheckSize(self.devusbsda2)
			self.sda4 = self.CheckSize(self.devusbsda4)
			self.msgsda1 = "/dev/sda1 : %3.01f MB Frei" % self.sda1
			self["SDA1"].setText(self.msgsda1)
			self.msgsda2 = "/dev/sda2 : %3.01f MB Frei" % self.sda2
			self["SDA2"].setText(self.msgsda2)
			if self.sda4 > 100000:
				#show GB
				self.sda4 = self.sda4/1024
				self.msgsda4 = "/dev/sda4 : %3.01f GB Frei" % self.sda4
				self["SDA4"].setText(self.msgsda4)
			else:
				self.msgsda4 = "/dev/sda4 : %3.01f MB Frei" % self.sda4
				self["SDA4"].setText(self.msgsda4)
			
		if self.sdb:
			self.devusbsdb1 = self.FindMountDev('/media/sdb1')
			self.sdb1 = self.CheckSize(self.devusbsdb1)
			if self.sdb1 > 100000:
				self.sdb1 = self.sdb1/1024
				self.msgsdb1 = "/dev/sdb1 : %3.01f GB Frei" % self.sdb1
				self["SDB1"].setText(self.msgsdb1)
			else:
				self.msgsdb1 = "/dev/sdb1 : %3.01f MB Frei" % self.sdb1
				self["SDB1"].setText(self.msgsdb1)

		if self.sdc:
			self.devusbsdc1 = self.FindMountDev('/media/sdc1')
			self.sdc1 = self.CheckSize(self.devusbsdc1)
			if self.sdc1 > 100000:
				self.sdc1 = self.sdc1/1024
				self.msgsdc1 = "/dev/sdc1 : %3.01f GB Frei" % self.sdc1
				self["SDC1"].setText(self.msgsdc1)
			else:
				self.msgsdc1 = "/dev/sdc1 : %3.01f MB Frei" % self.sdc1
				self["SDC1"].setText(self.msgsdc1)
			
		if self.sdd:
			self.devusbsdd1 = self.FindMountDev('/media/sdd1')
			self.sdd1 = self.CheckSize(self.devusbsdd1)
			if self.sdd1 > 100000:
				self.sdd1 = self.sdd1/1024
				self.msgsdd1 = "/dev/sdd1 : %3.01f GB Frei" % self.sdd1
				self["SDD1"].setText(self.msgsdd1)
			else:
				self.msgsdd1 = "/dev/sdd1 : %3.01f MB Frei" % self.sdd1
				self["SDD1"].setText(self.msgsdd1)
			
		if self.share:
			if self.GB:
				self.msgshare = self.sharepoint + ' : %3.01f GB Frei' % self.sharesize
				self["share"].setText(self.msgshare)
			else:
				self.msgshare = self.sharepoint + ' : %3.01f MB Frei' % self.sharesize
				self["share"].setText(self.msgshare)
		else:
			self.msgshare = ""

	def FindUSBDev(self, dev):
		diskstats = open("/proc/diskstats", 'r')
		for line in diskstats:
			result = line.strip().split()
			if result[2] == dev:
				diskstats.close()
				return dev
		diskstats.close()
		return None
	      
	def FindMountDev(self, dir):
		mounts = open("/proc/mounts", 'r')
		for line in mounts:
			result = line.strip().split()
			if result[1] == dir:
				mounts.close()
				return dir
		mounts.close()
		return None
	      
	def CheckSize(self, find):
		try:
			f = os.statvfs(find)
			return (f.f_bfree * f.f_frsize)/1024/1024
		except:
			return 0

	def logfile(self):
		try:
			# can file open, del
			f = open('/tmp/SysInfo.log','r')
			f.close()
			os.system('rm /tmp/SysInfo.log')
		except:
			# not open not del
			print "File can't open, not present, no del"

		try:
			# write file
			f = open('/tmp/SysInfo.log','w')
			f.write('          SystemInformation' + '\n\n' + 'Tuner/Kanal Information :' + '\n' + '    ' + self.msgservicenamecomplete + '\n' + '    ' + self.msgsnrValue + '\n' + '    ' + self.msgsignalQuality + '\n' + '    ' + self.msgsignalPower + '\n' + '    ' + self.msgfrontendStatus + '\n' + '    ' + self.msgsignalQualitydB + '\n' + '    ' + self.msgbitErrorRate + '\n' + '    ' + self.msglockState + '\n' + '    ' + self.crypt + '\n' + '    ' + self.msgfrontendNumber + '\n' + '    ' + self.femsgservice + '\n\n' + 'Image Information :' + '\n' + '    ' + self.msgVersion + '    ' + self.msgOSV + '    ' + self.msgMHZ + '    ' + self.msgram + '\n' + '    ' + self.msgswap + '\n' + '    ' + self.msgbuffer + '\n' + '    ' + self.msgCPUUSED + '\n' + '    ' + self.size + '\n\n' + 'Festplatten :' + '\n' + '    ' + self.msgsda1 + '\n' + '    ' + self.msgsda2 + '\n' + '    ' + self.msgsda4 + '\n' + '    ' + self.msgsdb1 + '\n' + '    ' + self.msgshare + '\n' + '    ' + self.msgsdc1 + '\n' '    ' + self.msgsdd1 + '\n')
			f.close()
			self.session.open(MessageBox, ("/tmp/SysInfo.log wurde erstellt !!!"), type = MessageBox.TYPE_INFO,timeout = 20 )
		except:
			# on error pass and print error
			print "ERROR file not Write"
			self.session.open(MessageBox, ("/tmp/SysInfo.log konnte nicht erstellt werden !!!"), type = MessageBox.TYPE_INFO,timeout = 20 )
			pass

	def cancel(self):
		print "[SystemInfo Close] - cancel\n"
		self.AutoLoadTimer.stop
		self.close()

###########################################################################

def main(session,**kwargs):
	session.open(SystemInfo)

###########################################################################

def Plugins(**kwargs):
	return PluginDescriptor(
				name="SystemInfo", 
				description = "SystemInfo", 
				where = PluginDescriptor.WHERE_EXTENSIONSMENU, 
				icon="../ihad_tut.png",fnc=main)
