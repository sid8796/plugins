#
#
# Write by Ducktrick
# 	(c)2014
#
# Mit diesen plugin ist es moeglich auch Files zu oeffnen 
# die nicht als jpg zur verfuegung stehen.
# es ist fuer eine WebCam enstanden die das bildfile als 
# images.cgi ausgegeben hat. der link mit endung cgi liess
# sich mit keinen Webcam Viewer oeffnen.
# Bei diesen wird das images.cgi gedownloadet und
# anschliessend geoeffnet
# Egal welche fileendung der Link hat, das resultat heisst
# immer image.jpg und wird versucht zu oeffnen !!!
# weboberflaechen mit user und password koennen wie folgt
# verwendet werden... z.B. so...
# http://Benutzername:Password@my.dyndns.org/images/images.cgi
# ( am einfachsten ist es an die Links zu kommen wenn ihr euch
# im Webif der Cam den Quelltext der Bildseite anzeigen lasst )
# das ganze wird anschliessend im Extensions Menue als plugin
# angezeigt, bei der auswahl wird das Bild in einen format
# von 320x240 in der oberen Linken ecke ausgegeben und alle
# 3 sec erneuert. :D
# zusaetzlich kann auf den Tasten Rechts Links fals verfuegbar
# und unterstuetzt von der Kamera diese geschwenkt werden ;)
# dazu muessen die links der Buttens vom Webif der Cam in der
# xml angeben werden, sollte keine verbindung zum link zustande 
# kommen bekommt ihr beim tastendruck eine Message auf dem TV
#
# Viel Spass mit dem Plugin

import os
import urllib
import libxml2
import time

from Screens.Screen import Screen
from Components.Label import Label
from Components.Pixmap import Pixmap
from Components.AVSwitch import AVSwitch
from Components.ActionMap import ActionMap
from Plugins.Plugin import PluginDescriptor
from enigma import ePicLoad
from enigma import eTimer
from Screens.MessageBox import MessageBox

from FullScreenPicture import ViewFull
from urlparse import urlparse, urlunparse

xmlfile = libxml2.parseFile( '/usr/lib/enigma2/python/Plugins/Extensions/MyViewer/cam.xml' )

# parse Bild url
bild = xmlfile.xpathEval('*/cam/url')
url = bild[0]

# parse control url
control = xmlfile.xpathEval('*/cam/control')
control = control[0]

url=(url.prop('Bild'))
control=(control.prop('Control'))

# definiert das image file
filename = "/usr/lib/enigma2/python/Plugins/Extensions/MyViewer/picture/image.jpg"
URL = '%s' % url
CONTROL = '%s' % control

# FIXME
parsurl = URL.strip()
parsed = urlparse(parsurl)
scheme = parsed[0]
if scheme == 'rtsp':
	rtmp=0
else:
	rtmp=1

class PictureScreen ( Screen ):
	skin = """
		<screen name="PictureScreen" position="0,0" size="320,180" title="Picture Screen" backgroundColor="#002C2C39" flags="wfNoBorder">
			<widget name="myPic" position="0,0" size="320,180" zPosition="1" alphatest="on" />
		</screen>"""
		
	def __init__ (self, session, picPath = None):
		Screen.__init__(self, session)
		print "[PictureScreen] __init__\n"
		self.picPath=filename
		self.Scale=AVSwitch().getFramebufferScale()
		self.AutoLoadTimer = eTimer()
		self.AutoLoadTimer.timeout.get().append(self.ShowPicture)
		self.PicLoad=ePicLoad()
		self["myPic"]=Pixmap()
		self["myActionMap"]=ActionMap(["WizardActions"],
			{
			"left":self.left,
			"right":self.right,
			"up":self.up,
			"down":self.down,
			"ok":self.full,
			"back":self.cancel
			}, -1)

		self.PicLoad.PictureData.get().append(self.DecodePicture)
		self.onLayoutFinish.append(self.ShowPicture)
			
	def ShowPicture(self):
		if self.picPath is not None:
			self.PicLoad.setPara([
						self["myPic"].instance.size().width(),
						self["myPic"].instance.size().height(),
						self.Scale[0],
						self.Scale[1],0,1,"#002C2C39"])
			print "download Picture"
			if rtmp == 0:
				os.system("ffmpeg -y -an -i %s -vframes 2 /usr/lib/enigma2/python/Plugins/Extensions/MyViewer/picture/image.jpg" % URL)	
			else:
				os.system("wget -O /usr/lib/enigma2/python/Plugins/Extensions/MyViewer/picture/image.jpg %s" % URL)
			self.PicLoad.startDecode(self.picPath)
			self.AutoLoadTimer.start(int(3)*1000)
			
	def DecodePicture(self, PicInfo=""):
		if self.picPath is not None:
			ptr = self.PicLoad.getData()
			self["myPic"].instance.setPixmap(ptr)

	def left(self):
		try:
    			urllib.urlopen("%s?command=4" % CONTROL)
			time.sleep(.500)
    			urllib.urlopen("%s?command=5" % CONTROL)
		except:
    			self.session.open(MessageBox, _('Keine Verbindung zur Internet Addresse'), type = MessageBox.TYPE_INFO, timeout = 5 )
		self.ShowPicture()

	def right(self):
		try:
    			urllib.urlopen("%s?command=6" % CONTROL)
			time.sleep(.500)
    			urllib.urlopen("%s?command=7" % CONTROL)
		except:
    			self.session.open(MessageBox, _('Keine Verbindung zur Internet Addresse'), type = MessageBox.TYPE_INFO, timeout = 5 )
		self.ShowPicture()

	def up(self):
		try:
    			urllib.urlopen("%s?command=18" % CONTROL)
			time.sleep(.500)
    			urllib.urlopen("%s?command=19" % CONTROL)
		except:
    			self.session.open(MessageBox, _('Keine Verbindung zur Internet Addresse'), type = MessageBox.TYPE_INFO, timeout = 5 )
		self.ShowPicture()

	def down(self):
		try:
    			urllib.urlopen("%s?command=16" % CONTROL)
			time.sleep(.500)
    			urllib.urlopen("%s?command=17" % CONTROL)
		except:
    			self.session.open(MessageBox, _('Keine Verbindung zur Internet Addresse'), type = MessageBox.TYPE_INFO, timeout = 5 )
		self.ShowPicture()

	def full(self):
		self.session.open(ViewFull)

	def cancel(self):
		print "[PictureScreen] - cancel\n"
		if os.path.isfile(filename):
			print "Delete file"
			os.system("rm %s" % filename)
		else:
			print "file not found in picture/"
		
		self.close()

###########################################################################

def main(session,**kwargs):
	session.open(PictureScreen)

###########################################################################

def Plugins(**kwargs):
	return PluginDescriptor(
				name="WebCam Mini Viewer", 
				description = "zum anzeigen von WebCam Pictures", 
				where = PluginDescriptor.WHERE_EXTENSIONSMENU, 
				icon="../ihad_tut.png",fnc=main)
