#######################################################################
#
#    Plugin for Edison-Enigma2
#    coded by Ducktrick (c)2014
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#######################################################################

from Screens.Screen import Screen
from Screens.MessageBox import MessageBox
from Components.ConfigList import ConfigList, ConfigListScreen
from Components.config import *
from Components.ActionMap import ActionMap
from Components.Label import Label
from Plugins.Plugin import PluginDescriptor
from Tools import Notifications
import Screens.Standby
import os

config.plugins.Standby = ConfigSubsection()
config.plugins.Standby.boot = ConfigYesNo(default=False) # Yes no for Bootup in Standby

def sessionAutostart(session, **kwargs):
	session.open(BoxStandbyAutoMain)

def Plugins(path, **kwargs):
	return [ PluginDescriptor(name="StandbyBootUp", description=_("StandbyBootUp"), where = PluginDescriptor.WHERE_PLUGINMENU, fnc=main),
		PluginDescriptor(where=[PluginDescriptor.WHERE_SESSIONSTART], fnc=sessionAutostart)  
   ]

def main(session,**kwargs):
	session.open(SetupMenu)

class SetupMenu(Screen, ConfigListScreen):

	skin = """
	<screen name="Config BoxBootUp in Standby" position="center,center" size="500,160" title="" backgroundColor="#31000000" >

			<widget name="config" position="10,10" size="480,115" zPosition="1" transparent="0" backgroundColor="#31000000" scrollbarMode="showOnDemand" />
			<widget name="key_green" position="0,125" zPosition="2" size="250,35" valign="center" halign="center" font="Regular;22" transparent="1" foregroundColor="green" />
			<widget name="key_red" position="250,125" zPosition="2" size="250,35" valign="center" halign="center" font="Regular;22" transparent="1" foregroundColor="red" />

	</screen>"""
	
	def __init__(self, session):
		Screen.__init__(self, session)
		
		self["actions"] = ActionMap(["SetupActions", "ColorActions", "OkCancelActions"],
			{
				"ok": self.keySave,
				"save": self.keySave,
				"cancel": self.keyCancel,
				"left": self.keyLeft,
				"right": self.keyRight
			}, -2)

		self["key_green"] = Label(_("Save"))
		self["key_red"] = Label(_("Cancel"))

		self.runSetup()

	def runSetup(self):
		self.list = []
		self.list.append(getConfigListEntry(_("Box in standby Starten:"), config.plugins.Standby.boot))
		self["config"] = ConfigList(self.list)

	def keyLeft(self):
		self["config"].handleKey(KEY_LEFT)

	def keyRight(self):
		self["config"].handleKey(KEY_RIGHT)

	def keySave(self):
		config.plugins.Standby.save()
		self.close()

	def keyCancel(self):
		self.close()

class BoxStandbyAutoMain(Screen):
	def __init__(self, session):
		Screen.__init__(self, session)
		if config.plugins.Standby.boot.value == True:
			Notifications.AddNotificationWithCallback(self.standby, MessageBox, _("     Standby on Boot Active     \n     receiver goto standby!     "), timeout = 10)			
		else:
			pass

	def standby(self, answer):
		if answer is not None:
			if answer and not Screens.Standby.inStandby:
				Notifications.AddNotification(Screens.Standby.Standby)
