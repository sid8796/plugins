##
## Permanent Info
## by MiNiMaG
##
from Components.ActionMap import ActionMap
from Components.config import config, ConfigInteger, ConfigSubsection, ConfigYesNo
from Components.MenuList import MenuList
from enigma import ePoint, eTimer, getDesktop
from os import environ
from Plugins.Plugin import PluginDescriptor
from Screens.Screen import Screen
from Tools.Directories import resolveFilename
import gettext

##############################################################################

config.plugins.PermanentInfo = ConfigSubsection()
config.plugins.PermanentInfo.enabled = ConfigYesNo(default=True)
config.plugins.PermanentInfo.fortschritt = ConfigYesNo(default=False)
config.plugins.PermanentInfo.position_x = ConfigInteger(default=0)
config.plugins.PermanentInfo.position_y = ConfigInteger(default=600)

##############################################################################

if config.plugins.PermanentInfo.fortschritt.value == False:
	SKIN = """
		<screen position="0,0" size="1280,24" zPosition="-2" title="Permanent Info" flags="wfNoBorder">
			<widget source="session.CurrentService" render="Label" position="50,0" size="60,24" font="Regular;18" halign="center" valign="center" transparent="1">
				<convert type="ExtendedServiceInfo">ServiceNumber</convert>
			</widget>
			<widget source="session.Event_Now" render="Label" position="140,0" size="550,24" noWrap="1" font="Regular;18" halign="left" valign="center" transparent="1">
				<convert type="EventName">Name</convert>
			</widget>
				<widget source="session.Event_Now" render="Label"		position="700,0" size="70,24" font="Regular;18" halign="right"	backgroundColor="#25080808" transparent="1">
				<convert type="EventTime">StartTime</convert>
				<convert type="ClockToText">Default</convert>
			</widget>
			<widget source="session.Event_Now" render="Label"		position="970,0" size="70,24" font="Regular;18" halign="left"	backgroundColor="#25080808" transparent="1">
				<convert type="EventTime">EndTime</convert>
				<convert type="ClockToText">Default</convert>
			</widget>
			<widget source="session.Event_Now" render="Progress" 	position="780,6" size="180,12" borderWidth="1" borderColor="#cccccc" backgroundColor="#2518252e" transparent="1">
				<convert type="EventTime">Progress</convert>
			</widget>
			<widget source="global.CurrentTime" render="Label" position="1150,0" size="60,24" font="Regular;18" valign="center" transparent="1" halign="center" >
				<convert type="ClockToText">Default</convert>
			</widget>
		</screen>"""
else:
	SKIN = """
		<screen position="0,0" size="1280,4" zPosition="-2" title="Permanent Info" flags="wfNoBorder">
			<widget pixmap="/usr/lib/enigma2/python/Plugins/Extensions/PermanentInfo/color/00c3461b.png" position="0,0" render="Progress" size="1280,4" source="session.Event_Now" transparent="1" zPosition="7">
				<convert type="EventTime">Progress</convert>
			</widget>
		</screen>"""

##############################################################################

def _(txt):
	t = gettext.dgettext("PermanentInfo", txt)
	if t == txt:
		t = gettext.gettext(txt)
	return t

##############################################################################

class TitleScreen(Screen):
	def __init__(self, session, parent=None):
		Screen.__init__(self, session, parent)
		self.onLayoutFinish.append(self.setScreenTitle)

	def setScreenTitle(self):
		self.setTitle(_("Permanent Info"))

##############################################################################

class PermanentInfoScreen(Screen):
	def __init__(self, session):
		Screen.__init__(self, session)
		self.skin = SKIN
		self.onShow.append(self.movePosition)

	def movePosition(self):
		if self.instance:
			self.instance.move(ePoint(config.plugins.PermanentInfo.position_x.value, config.plugins.PermanentInfo.position_y.value))

##############################################################################

class PermanentInfo():
	def __init__(self):
		self.dialog = None

	def gotSession(self, session):
		self.dialog = session.instantiateDialog(PermanentInfoScreen)
		self.showHide()

	def changeVisibility(self):
		if config.plugins.PermanentInfo.enabled.value:
			config.plugins.PermanentInfo.enabled.value = False
		else:
			config.plugins.PermanentInfo.enabled.value = True
		if config.plugins.PermanentInfo.fortschritt.value:
			config.plugins.PermanentInfo.fortschritt.value = False
		else:
			config.plugins.PermanentInfo.fortschritt.value = True
		config.plugins.PermanentInfo.fortschritt.save()
		config.plugins.PermanentInfo.enabled.save()
		self.showHide()
		
	def changeVisibilityState(self):
		if config.plugins.PermanentInfo.fortschritt.value:
			config.plugins.PermanentInfo.fortschritt.value = False
		else:
			config.plugins.PermanentInfo.fortschritt.value = True
		config.plugins.PermanentInfo.fortschritt.save()
		self.skin = SKIN
		self.showHide()

	def showHide(self):
		if config.plugins.PermanentInfo.enabled.value:
			self.dialog.show()
		else:
			self.dialog.hide()

pInfo = PermanentInfo()

##############################################################################

class PermanentInfoPositioner(Screen):
	def __init__(self, session):
		Screen.__init__(self, session)
		self.skin = SKIN
		
		self["actions"] = ActionMap(["WizardActions"],
		{
			"left": self.left,
			"up": self.up,
			"right": self.right,
			"down": self.down,
			"ok": self.ok,
			"back": self.exit
		}, -1)
		
		desktop = getDesktop(0)
		self.desktopWidth = desktop.size().width()
		self.desktopHeight = desktop.size().height()
		
		self.moveTimer = eTimer()
		self.moveTimer.timeout.get().append(self.movePosition)
		self.moveTimer.start(50, 1)

	def movePosition(self):
		self.instance.move(ePoint(config.plugins.PermanentInfo.position_x.value, config.plugins.PermanentInfo.position_y.value))
		self.moveTimer.start(50, 1)

	def left(self):
		value = config.plugins.PermanentInfo.position_y.value
		value -= 10
		if value < 0:
			value = 0
		config.plugins.PermanentInfo.position_y.value = value

	def up(self):
		value = config.plugins.PermanentInfo.position_y.value
		value -= 1
		if value < 0:
			value = 0
		config.plugins.PermanentInfo.position_y.value = value

	def right(self):
		value = config.plugins.PermanentInfo.position_y.value
		value += 10
		if value > self.desktopWidth:
			value = self.desktopWidth
		config.plugins.PermanentInfo.position_y.value = value

	def down(self):
		value = config.plugins.PermanentInfo.position_y.value
		value += 1
		if value > self.desktopHeight:
			value = self.desktopHeight
		config.plugins.PermanentInfo.position_y.value = value

	def ok(self):
		config.plugins.PermanentInfo.position_y.save()
		self.close()

	def exit(self):
		config.plugins.PermanentInfo.position_y.cancel()
		self.close()

##############################################################################

class PermanentInfoMenu(TitleScreen):
	skin = """
		<screen position="150,235" size="420,105" title="Permanent Info">
			<widget name="list" position="10,10" size="400,85" />
		</screen>"""

	def __init__(self, session):
		TitleScreen.__init__(self, session)
		self.session = session
		self["list"] = MenuList([])
		self["actions"] = ActionMap(["OkCancelActions"], {"ok": self.okClicked, "cancel": self.close}, -1)
		self.onLayoutFinish.append(self.showMenu)

	def showMenu(self):
		list = []
		if config.plugins.PermanentInfo.enabled.value:
			list.append(_("Deaktiviere PermanentInfo"))
		else:
			list.append(_("Aktiviere PermanentInfo"))
		if config.plugins.PermanentInfo.fortschritt.value:
			list.append(_("Deaktiviere single Vortschritt Status"))
		else:
			list.append(_("Aktiviere single Vortschritt Status"))
		list.append(_("Verschieben"))
		self["list"].setList(list)
		
	def positionerCallback(self, callback=None):
		pInfo.showHide()

	def okClicked(self):
		sel = self["list"].getCurrent()
		if sel == _("Deaktiviere PermanentInfo") or sel == _("Aktiviere PermanentInfo"):
			if pInfo.dialog is None:
				pInfo.gotSession(self.session)
			pInfo.changeVisibility()
			self.showMenu()
		elif sel == _("Deaktiviere single Vortschritt Status") or sel == _("Aktiviere single Vortschritt Status"):
			if pInfo.dialog is None:
				pInfo.gotSession(self.session)
			pInfo.changeVisibilityState()
			self.showMenu()
		else:
			if pInfo.dialog is None:
				pInfo.gotSession(self.session)
			pInfo.dialog.hide()
			self.session.openWithCallback(self.positionerCallback, PermanentInfoPositioner)

##############################################################################

def sessionstart(reason, **kwargs):
	if reason == 0:
		pInfo.gotSession(kwargs["session"])

def main(session, **kwargs):
	session.open(PermanentInfoMenu)

##############################################################################

def Plugins(**kwargs):
	return [
		PluginDescriptor(where=[PluginDescriptor.WHERE_SESSIONSTART], fnc=sessionstart),
		PluginDescriptor(name=_("Permanent Info"), description=_("Zeigt dauerhaft Infos zur aktuellen Sendung"), where=PluginDescriptor.WHERE_PLUGINMENU, fnc=main)]
