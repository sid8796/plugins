#
#
# Write by Ducktrick
# 	(c)2014
#

import os
import time
import datetime

from Screens.Screen import Screen
from Components.Label import Label
from Components.ActionMap import ActionMap
from Plugins.Plugin import PluginDescriptor
from Tools import Notifications
from enigma import eTimer
from Screens.MessageBox import MessageBox
from Components.ConfigList import ConfigList
from Components.config import *

from Config import *

class Counter( Screen ):
	skin = """
		<screen name="counter" position="center,center" size="520,180" title="Counter" backgroundColor="#002C2C39" flags="wfNoBorder">
			<widget name="myCounter" position="0,0" size="520,180" zPosition="1" alphatest="on" />
			<widget name="myText" position="10,10" size="500,160" valign="center" halign="center" zPosition="2"  foregroundColor="white" font="Regular;22"/>
		</screen>"""
		
	def __init__ (self, session, picPath = None):
		Screen.__init__(self, session)
		print "[myCounter] __init__\n"
		self["myText"] = Label()
      		self.msg = "Lade..."			
		self.AutoLoadTimer = eTimer()
		self.AutoLoadTimer.timeout.get().append(self.ShowCounter)
		self["myActionMap"] = ActionMap(["MinuteInputActions", "MenuActions"],
		{
			"cancel": self.cancel,
			"menu": self.config
		}, -1)

		self.onLayoutFinish.append(self.ShowCounter)
		
	def config(self):
		print "Open Configuration"
		self.session.open(ConfigCountdown)
		
	def error(self, result):
		if result:
			self.config
		else:
			self.cancel
			
	def finish(self, answer):
		if result:
			self.config
		else:
			self.cancel
			
	def ShowCounter(self):
		i = datetime.datetime.now()
		self.tag = i.day
		self.stunde = i.hour
		self.minute = i.minute
		self.secunde = i.second

		# Monat bis zu dem gezaehlt wird
		self.stopmonth = config.plugins.Countdown_globalsettings.setmonth.value
		# Tag zu dem gezaehlt wird
		self.firstday = config.plugins.Countdown_globalsettings.setday.value
		# Endjahr
		self.endyear = config.plugins.Countdown_globalsettings.setyear.value

		# find aktuell Month
		self.month = i.month
		# finde aktuelles Jahr
		self.year = i.year
		# find month to end
		if self.stopmonth == self.month:
			self.releasemonth = 0
		else:
			self.releasemonth = self.stopmonth - self.month - 1
			
		try:
			if self.stopmessage < 1:
				print "wait of koreckt date"
			else:
				if self.month > self.stopmonth and self.endyear == self.year:
					self.stopmessage = 0
					self.session.openWithCallback(self.error, MessageBox, _("Bitte geben Sie einen koreckten Monat in der Zukunft liegend an "), MessageBox.TYPE_YESNO)
				else:
					self.stopmessage = 1
		except:
			self.stopmessage = 1

		# Find tage in Month
		monattage = [30,28,30,31,30,31,30,31,30,31,30,31]
		tage = range(1,monattage[self.month-1]+1)
		tage = tage[-1]
		self.tage = tage - self.tag
		self.releasestunde = 23 - self.stunde
		self.releaseminute = 60 - self.minute
      		self.releasesecunde = 60 - self.secunde
      		if self.stopmessage < 1:
			print "wait of koreckt date"
		else:
			if self.firstday == self.tag and self.month == self.stopmonth and self.releasestunde == 0 and self.releaseminute == 60 and self.releasesecunde == 60:
				self.session.openWithCallback(self.finish, MessageBox, _("Der Countdown ist abgelaufen, neuen Countdown starten ? "), MessageBox.TYPE_YESNO)
      		if self.releasesecunde == 60:
			self.releasesecunde = 59
      		if self.releaseminute == 60:
			self.releaseminute = 59
      		if self.releasestunde < 0:
			self.releasestunde = 23
		if self.stopmonth == self.month and self.tag == config.plugins.Countdown_globalsettings.setday.value:
			self.releasemonth = 0
			self.tage = 0
		elif ((self.tage + self.firstday) > tage):
			self.releasemonth = self.releasemonth + 1
			if ((self.tage + self.firstday - self.tag - tage) < 0):
				self.tage = self.tage + self.firstday - self.tag
			else:
				self.tage = self.tage + self.firstday - self.tag - tage
		else:
			self.tage = self.tage + self.firstday
      		self.msg = (("Es verbleiben bis zum %s.%s.%s \n%s Monate, %s Tage, %sh:%sm:%ss\n") % (self.firstday, self.stopmonth, self.endyear, self.releasemonth, self.tage, self.releasestunde, self.releaseminute, self.releasesecunde))
		self.text = "%s" % self.msg
		self["myText"].setText(self.text)
		if self.stopmessage == 0:
			print "Stopped Timer"
		else:
			self.AutoLoadTimer.start(int(1)*1000)
			

	def cancel(self):
		print "[Counter Close] - cancel\n"
		self.close()

###########################################################################

def main(session,**kwargs):
	session.open(Counter)

###########################################################################

def Plugins(**kwargs):
	return PluginDescriptor(
				name="CountDown", 
				description = "countdown", 
				where = PluginDescriptor.WHERE_EXTENSIONSMENU, 
				icon="../ihad_tut.png",fnc=main)
