from enigma import eTimer
from Screens.Screen import Screen
from Screens.Standby import TryQuitMainloop
from Screens.ServiceInfo import ServiceInfoList, ServiceInfoListEntry
from Components.ActionMap import ActionMap, NumberActionMap
from Components.Pixmap import Pixmap, MovingPixmap
from Components.Label import Label
from Screens.MessageBox import MessageBox
from Components.Sources.List import List
from Components.ConfigList import ConfigList, ConfigListScreen
from Components.Sources.StaticText import StaticText
from Components.MenuList import MenuList
from Components.Console import Console
from Components.ScrollLabel import ScrollLabel
from Components.config import *

from Components.Button import Button

from Tools.Directories import resolveFilename, fileExists, pathExists, createDir, SCOPE_MEDIA
from Components.FileList import FileList
from Plugins.Plugin import PluginDescriptor

config.plugins.Countdown_globalsettings = ConfigSubsection()
config.plugins.Countdown_globalsettings.setday = ConfigInteger(01, (01,31))
config.plugins.Countdown_globalsettings.setmonth = ConfigInteger(01, (01,12))
config.plugins.Countdown_globalsettings.setyear = ConfigInteger(2014, (2014,3000))

class ConfigCountdown(Screen):
	skin = """
		<screen position="center,center" size="500,180" title="Global Settings" name="Config">
			<widget name="configlist" position="10,10" size="450,100" />
			<widget name="key_green" position="10,108" zPosition="1" size="130,30" valign="center" halign="center" font="Regular;20" transparent="1" />
			<ePixmap position="10,111" zPosition="2" size="130,3" pixmap="/usr/lib/enigma2/python/Plugins/Extensions/TeamCS/skin/pic/green.png" transparent="1" alphatest="on" />
		</screen>"""

	def __init__(self, session):
		Screen.__init__(self, session)

		self["actions"] = NumberActionMap(["SetupActions","OkCancelActions"],
		{
			"ok": self.keyGREEN,
			"save": self.keyGREEN,
			"cancel": self.close,
			"left": self.keyLeft,
			"right": self.keyRight,
			"0": self.keyNumber,
			"1": self.keyNumber,
			"2": self.keyNumber,
			"3": self.keyNumber,
			"4": self.keyNumber,
			"5": self.keyNumber,
			"6": self.keyNumber,
			"7": self.keyNumber,
			"8": self.keyNumber,
			"9": self.keyNumber
		}, -1)
		self["key_green"] = Label(_("Save"))
		
		self.list = []
		self.list.append(getConfigListEntry(_("Countdown Tag eingeben"), config.plugins.Countdown_globalsettings.setday))
		self.list.append(getConfigListEntry(_("Countdown Monat eingeben"), config.plugins.Countdown_globalsettings.setmonth))
		self.list.append(getConfigListEntry(_("Countdown Jahr eingeben"), config.plugins.Countdown_globalsettings.setyear))
		self["configlist"] = ConfigList(self.list)

	def keyLeft(self):
		self["configlist"].handleKey(KEY_LEFT)

	def keyRight(self):
		self["configlist"].handleKey(KEY_RIGHT)

	def keyNumber(self, number):
		self["configlist"].handleKey(KEY_0 + number)

	def keyGREEN(self):
		config.plugins.Countdown_globalsettings.save()
		self.close()