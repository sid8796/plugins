��    J      l  e   �      P  	   Q  K   [  G   �  4   �     $  	   (     2     O     X  
   `     k     �     �  J   �     �  )   �     '     3     9  
   @     K  "   O     r     u     �  
   �     �     �     �     �     �  %   �     	     	     	      "	     C	  '   S	     {	     �	     �	  &   �	     �	     �	     �	  $   
     9
     N
     b
     j
     q
     x
     �
     �
     �
     �
     �
     �
     �
  	   �
  	   �
     �
     �
     �
  
   �
     
                '     .     @     H     _  �  f     �  U   �  D   I  L   �     �     �     �               )  #   6     Z  	   b  `   l     �  0   �          0     7  
   >     I  (   O     x     {     �  
   �     �  #   �     �  	   �     �  *        .     6     <  #   H     l  +   |     �      �     �  '   �          )     @  !   X     z     �     �     �     �  
   �     �     �     �     �     �     �               -     6     E  	   Q  
   [     f     r          �     �     �     �     �     D       4           C   G                 .   2   %   F          ?   
              )   J   	             5                     <      !   A       $   '      6      ;   >          8   &      #   1         -       +                           H           *   3      B   0   @       I   7          E   :      (   9              /         "          =                 ,        <unknown> A finished record timer wants to set your
Dreambox to standby. Do that now? A finished record timer wants to shut down
your Dreambox. Shutdown now? A timer failed to record!
Disable TV and try again?
 Add Add timer Control your EPG and more... Edit Off Edit On Event Info Exit on TV <-> Radio switch: Friday General In order to record a timer, the TV was switched to the recording service!
 Increase list item height: Limit search results to bouquet services: List style: Lists Monday New Search Now Number of upcoming events to show: OK Outdated off Outdated on Prime Time Prime time: Progress bar style: Remember last tab: Remove Remove timer Replace InfoBar single and multi EPG: Saturday Search Search for: Select running service on start: Set search term Show blinking picon for running timers: Show duration: Show event information: Show list numbers: Show multi colored begin/remain times: Show picons: Show service name: Show short description: Show text input help for epg search: Show timer messages: Show video picture: Similar Single Sunday Thursday Timer Today Tomorrow Tuesday Unknown date Upcoming Use picons (50x30) from: Use skin: Wednesday about to start disabled done! four parts gradient recording... return simple single line style waiting with short description zapped Project-Id-Version: MerlinEPGCenter
POT-Creation-Date: 2011-10-09 11:05+CEST
PO-Revision-Date: 
Last-Translator: Shaderman <shaderman@dreambox-tools.info>
Language-Team: Shaderman <shaderman@dreambox-tools.info>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
 <unbekannt> Ein Aufnahmetimer will Ihre Dreambox in den Standby-Modus schalten.
Jetzt ausführen? Ein Aufnahmetimer will Ihre Dreambox ausschalten.
Jetzt ausschalten? Timeraufnahme fehlgeschlagen.
Fernsehprogramm ändern und erneut versuchen?
 Hinzufügen Timer setzen Mehr als nur ein EPG... Bearbeiten an Bearbeiten aus Sendungsinfo Beenden beim Wechsel TV <--> Radio: Freitag Allgemein Um die Timeraufnahme durchführen zu können, wurde auf den aufzunehmenden Sender umgeschaltet!
 Zeilenhöhe vergrößern: Reduziere Suchergebnisse auf Sender in Bouquets: Darstellung der Listen: Listen Montag Neue Suche Jetzt Anzahl der folgenden Sendungen anzeigen: Ok Vergangene aus Vergangene an Prime Time Prime time: Darstellung der Fortschrittsbalken: Speichere letzten Tab: Entfernen Entferne Timer Ersetze Einzel- und Multi-EPG der InfoBar: Samstag Suche Suche nach: Wähle laufende Sendung beim Start: Als Suchbegriff Zeige blinkende Picons für laufende Timer: Zeige Sendungsdauer: Zeige Informationen zur Sendung: Zeige Sendernummern: Zeige mehrfarbige Beginnzeit und Dauer: Zeige Picons: Zeige den Sendernamen: Zeige Kurzbeschreibung: Zeige Eingabehilfe bei EPG-Suche: Zeige Meldungen von Timern: Zeige Videobild: Ähnlich Einzel Sonntag Donnerstag Timer Heute Morgen Dienstag Unbekanntes Datum Nächste Verwende Picons (50x30) von: Verwende Skin: Mittwoch startet gleich deaktiviert erledigt! Vier Teile Farbverlauf nimmt auf... zurück Einfach Einzelne Zeile wartend Mit Kurzbeschreibung umgeschaltet 